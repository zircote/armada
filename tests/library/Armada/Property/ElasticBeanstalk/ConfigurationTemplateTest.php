<?php
namespace ArmadaTests\Property\ElasticBeanstalk;

/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class ConfigurationTemplateTest extends \PHPUnit_Framework_TestCase
{
    /**
     * The name of the configuration template.
     *
     * Type: String
     * Required: Yes
     *
     * @var
     */
    protected $templateName;
    /**
     * An optional description for this configuration.
     *
     * Type: String
     * Required: No
     *
     * @var
     */
    protected $description;
    /**
     * A list of OptionSettings for this Elastic Beanstalk configuration. For a complete list of Elastic Beanstalk
     * configuration options, see Option Values, in the AWS Elastic Beanstalk Developer Guide.
     *
     * Type: A list of OptionSettings.
     * Required: No
     *
     * @var
     */
    protected $optionSettings;
    /**
     * The name of an existing Elastic Beanstalk solution stack used by this configuration. A solution stack specifies
     * the operating system, architecture, and application server for a configuration template. It also defines
     * configuration options, their possible and default values. If SolutionStackName is not specified, the default
     * Elastic Beanstalk solution stack will be used.
     *
     * Type: String
     * Required: No
     *
     * @var
     */
    protected $solutionStackName;

}
