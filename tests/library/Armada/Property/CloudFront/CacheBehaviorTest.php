<?php
namespace ArmadaTests\Property;

/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 * Describes the Amazon CloudFront cache behavior when the requested URL matches a pattern. This is an embedded
 * property of the DistributionConfig type.
 *
 */
class CacheBehaviorTest extends \PHPUnit_Framework_TestCase
{

}
