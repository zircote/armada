<?php
namespace ArmadaTests\Property\AutoScaling;

/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class TagsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * The key name of the tag.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $key;
    /**
     * The value for the tag.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $value;
    /**
     * Set to true if you want AWS CloudFormation to copy the tag to EC2 instances that are launched as part of the
     * auto scaling group. Set to false if you want the tag attached only to the auto scaling group and not copied to
     * any instances launched as part of the auto scaling group.
     *
     * Required: Yes
     * Type: Boolean
     *
     * @var
     */
    protected $propagateAtLaunch;
}
