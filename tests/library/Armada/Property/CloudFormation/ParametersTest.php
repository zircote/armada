<?php
namespace ArmadaTests\Property\CloudFormation;

/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 *
 * The Parameters type contains a set of value pairs that represent the parameters that will be passed to the template
 * used to create an AWS::CloudFormation::Stack resource. Each parameter has a name corresponding to a parameter
 * defined in the embedded template and a value respresenting the value that you want to set for the parameter. For
 * example, the sample template EC2ChooseAMI.template contains the following Parameters section:
 * <code>
 * "Parameters" : {
 *    "InstanceType" : {
 *       "Type" : "String",
 *       "Default" : "m1.small",
 *       "Description" : "EC2 instance type, e.g. m1.small, m1.large, etc."
 *    },
 *    "WebServerPort" : {
 *       "Type" : "String",
 *       "Default" : "80",
 *       "Description" : "TCP/IP port of the web server"
 *    },
 *    "KeyName" : {
 *       "Type" : "String",
 *       "Description" : "Name of an existing EC2 KeyPair to enable SSH access to the web server"
 *    }
 * }
 * </code>
 */
class ParametersTest extends \PHPUnit_Framework_TestCase
{

}
