<?php

namespace ArmadaTests\Resource\AWS\IAM;

use Armada\Resource\AbstractResource;

class AccessKeyTest extends \PHPUnit_Framework_TestCase
{
    const NAME = 'AWS::IAM::AccessKey';
    /**
     * This value is specific to AWS CloudFormation and can only be incremented. Incrementing this value notifies AWS
     * CloudFormation that you want to rotate your access key. When you update your stack, AWS CloudFormation will
     * replace the existing access key with a new key.
     *
     * Required: No
     * Type: Integer
     *
     * Update requires: replacement
     *
     * @var integer
     */
    protected $serial;
    /**
     * The status of the access key.
     *
     * Required: Yes
     * Type: String
     *
     * Valid values: "Active" or "Inactive"
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $status;
    /**
     * The name of the user that the new key will belong to.
     *
     * Required: Yes
     * Type: String
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $username;

}

