<?php

namespace ArmadaTests\Resource\AWS\IAM;

use Armada\Resource\AbstractResource;

class GroupTest extends \PHPUnit_Framework_TestCase
{
    const NAME = 'AWS::IAM::Group';
    /**
     * The path to the group. For more information about paths, see Identifiers for IAM Entities in Using IAM.
     *
     * Required: No
     * Type: String
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $path;
    /**
     * The policies to be added to the group. For information about policies, see Overview of Policies in Using IAM.
     *
     * Required: No
     * Type: List of AWS::IAM::Policy types
     *
     * Update requires: no interruption
     *
     * @var array(\Policy)
     */
    protected $policies = array();

}

