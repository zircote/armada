<?php

namespace ArmadaTests\Resource\AWS\EC2;

class SecurityGroupIngressTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Name of the EC2 security group to modify. This value can be a reference to an AWS::EC2::SecurityGroup resource or
     * the name of an existing EC2 security group.
     *
     * Type: String
     * Required: Can be used instead of GroupId for EC2 security groups.
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $groupName;
    /**
     * ID of the EC2 or VPC security group to modify. The group must belong to your account.
     *
     * Type: String
     * Required: Yes, for VPC security groups; can be used instead of GroupName for EC2 security groups
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $groupId;
    /**
     * IP protocol name or number. For valid values, see the IpProtocol parameter in AuthorizeSecurityGroupIngress
     *
     * Type: String
     * Required: Yes
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $ipProtocol;
    /**
     * Specifies a CIDR range.
     * For an overview of CIDR ranges, go to the Wikipedia Tutorial.
     *
     * Condition: If you specify SourceSecurityGroupName, do not specify CidrIp.
     * Type: String
     * Required: Conditional—if you specify SourceSecurityGroupName, do not specify CidrIp.
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $cidrIp;
    /**
     * Specifies the name of the Amazon EC2 Security Group to allow access or uses the Ref intrinsic function to refer
     * to the logical name of a security group defined in the same template.
     *
     * Type: String
     * Required: Conditional—if you specify CidrIp, do not specify SourceSecurityGroupName.
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $sourceSecurityGroupName;
    /**
     * Specifies the ID of the source Security Group or uses the Ref intrinsic function to refer to the logical ID of a
     * security group defined in the same template.
     *
     * Condition: If you specify CidrIp, do not specify SourceSecurityGroupId.
     * Type: String
     * Required: Conditional—if you specify CidrIp, do not specify SourceSecurityGroupId.
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $sourceSecurityGroupId;
    /**
     * Specifies the AWS Account ID of the owner of the Amazon EC2 Security Group specified in the
     * SourceSecurityGroupName property.
     *
     * Type: String
     * Required: Conditional—if you specify SourceSecurityGroupName and that security group is owned by a different
     * account than the account creating the stack, you must specify the SourceSecurityGroupOwnerId; otherwise, this
     * property is optional.
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $sourceSecurityGroupOwnerId;
    /**
     * Start of port range for the TCP and UDP protocols, or an ICMP type number. An ICMP type number of -1 indicates
     * a wildcard (i.e., any ICMP type number).
     *
     * Type: String
     * Required: Yes, for ICMP and any protocol that uses ports.
     *
     * Update requires: no interruption
     *
     * @var number
     */
    protected $fromPort;
    /**
     * End of port range for the TCP and UDP protocols, or an ICMP code. An ICMP code of -1 indicates a wildcard (i.e.,
     * any ICMP code).
     *
     * Type: String
     * Required: Yes, for ICMP and any protocol that uses ports.
     *
     * Update requires: no interruption
     *
     * @var number
     */
    protected $toPort;


}

