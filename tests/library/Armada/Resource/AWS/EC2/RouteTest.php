<?php

namespace ArmadaTests\Resource\AWS\EC2;

class RouteTest extends \PHPUnit_Framework_TestCase
{
    /**
     * The CIDR address block used for the destination match. For example, "0.0.0.0/0". Routing decisions are based on
     * the most specific match.
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $DestinationCidrBlock;
    /**
     * The ID of a gateway attached to your VPC. For example: "igw-eaad4883".
     *
     * Required: Conditional. You must provide only one of the following: a GatewayID, InstanceID, or NetworkInterfaceId.
     * Type: String
     *
     * @var string
     */
    protected $GatewayId;
    /**
     * The ID of a NAT instance in your VPC. For example, "i-1a2b3c4d".
     *
     * Required: Conditional. You must provide only one of the following: a GatewayID, InstanceID, or NetworkInterfaceId.
     * Type: String
     *
     * @var string
     */
    protected $InstanceId;
    /**
     * Allows the routing of network interface IDs.
     *
     * Required: Conditional. You must provide only one of the following: a GatewayID, InstanceID, or NetworkInterfaceId.
     * Type: String
     *
     * @var string
     */
    protected $NetworkInterfaceId;
    /**
     * The ID of the route table where the route will be added.
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $RouteTableId;

}

