<?php

namespace ArmadaTests\Resource\AWS\EC2;

/**
 *
 * Creates a set of DHCP options for your VPC.
 *
 * <code>
 * {
 *    "AWSTemplateFormatVersion" : "2010-09-09",
 *    "Resources" : {
 *       "myDhcpOptions" : {
 *          "Type" : "AWS::EC2::DHCPOptions",
 *          "Properties" : {
 *             "DomainName" : "example.com",
 *             "DomainNameServers" : ["AmazonProvidedDNS"],
 *             "NtpServers" : ["10.2.5.1"],
 *             "NetbiosNameServers" : ["10.2.5.1"],
 *             "NetbiosNodeType" : "2",
 *             "Tags" : [{"Key" : "foo", "Value" : "bar"}],
 *             }
 *          }
 *       }
 *    }
 * }
 * </code>
 *
 */
class DHCPOptionsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * A domain name of your choice.
     * Condition: At least one DHCP option must be specified
     * Example: example.com
     *
     * Required: Conditional
     * Type: String
     *
     * @var string
     */
    protected $domainName;
    /**
     * The IP address of a domain name server. You can specify up to four addresses.
     * Condition: At least one DHCP option must be specified
     * Valid values: IPv4 addresses
     * Example: 10.0.0.1
     *
     * Required: Conditional
     * Type: String List
     *
     * @var array(string)
     */
    protected $domainNameServers = array();
    /**
     * The IP address of a Network Time Protocol (NTP) server. You can specify up to four addresses.
     * Condition: At least one DHCP option must be specified
     * Valid values: IPv4 addresses
     * Example: 10.0.0.1
     *
     * Required: Conditional
     * Type: String
     *
     * @var string
     */
    protected $ntpServers;
    /**
     * The IP address of a NetBIOS name server. You can specify up to four addresses.
     * Condition: At least one DHCP option must be specified
     * Valid values: IPv4 addresses
     * Example: 10.0.0.1
     *
     * Required: Conditional
     * Type: String List
     *
     * @var array(string)
     */
    protected $netBiosNameServers = array();
    /**
     * Value indicating the NetBIOS node type (1, 2, 4, or 8). For more information about the values, go to RFC 2132.
     * We recommend you only use 2 at this time (broadcast and multicast are currently not supported).
     * Condition: At least one DHCP option must be specified
     *
     * Valid values: 1 | 2 | 4 | 8
     *
     * Required: Conditional
     * Type: Integer
     *
     * @var int
     */
    protected $netbiosNodeType;
    /**
     * The tags you want to attach to this resource.
     * For more information about tags, go to Using Tags in the Amazon Elastic Compute Cloud User Guide.
     * Update requires: no interruption
     *
     * Required: No
     * Type: List of EC2 Tags type
     *
     * @var \Tags
     */
    protected $tags;

}

