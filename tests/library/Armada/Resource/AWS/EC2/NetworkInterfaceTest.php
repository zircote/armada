<?php
namespace ArmadaTests\Resource\AWS\EC2;

/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class NetworkInterfaceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * The description of this network interface.
     *
     * Required: No
     * Type: String
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $description;
    /**
     * A list of security group IDs associated with this network interface.
     *
     * Required: No
     * Type: String.
     *
     * Update requires: no interruption
     *
     * @var
     */
    protected $groupSet;
    /**
     * IP address of the interface within the subnet.
     *
     * Required: No
     * Type: String
     * Update requires: replacement
     *
     * @var string
     */
    protected $privateIpAddress;
    /**
     * Flag indicating whether traffic to or from the instance is validated.
     *
     * Required: No
     * Type: Boolean
     *
     * Update requires: no interruption
     *
     * @var bool
     */
    protected $sourceDestCheck;
    /**
     * The ID of the subnet to associate with the network interface.
     *
     * Required: Yes
     * Type: String
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $subnetId;
    /**
     * A list of tags associated with this network interface.
     *
     * Required: No
     * Type: List of EC2 Tags.
     *
     * Update requires: no interruption
     *
     * @var
     */
    protected $tags;
}
