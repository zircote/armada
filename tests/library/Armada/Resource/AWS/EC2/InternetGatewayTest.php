<?php

namespace ArmadaTests\Resource\AWS\EC2;

class InternetGatewayTest extends \PHPUnit_Framework_TestCase
{
    /**
     * The tags you want to attach to this resource.
     * For more information about tags, go to Using Tags in the Amazon Elastic Compute Cloud User Guide.
     *
     * Update requires: no interruption
     *
     * @var array
     */
    protected $tags = array();
}

