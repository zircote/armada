<?php

namespace ArmadaTests\Resource\AWS\EC2;

class InstanceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Specifies the name of the availability zone in which the instance is located. If not specified, the default
     * availability zone for the region will be used.
     *
     * Required: No
     * Type: String
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $availabilityZone;
    /**
     * Specifies whether the instance can be terminated through the API (specify "true"), or not (specify "false").
     *
     * Required: No
     * Type: Boolean
     *
     * Update requires: no interruption
     *
     * @var bool
     */
    protected $disableApiTermination;
    /**
     * Specifies whether the instance is optimized for EBS I/O. This optimization provides dedicated throughput to
     * Amazon EBS and an optimized configuration stack to provide optimal EBS I/O performance. This optimization isn’t
     * available with all instance types. Additional usage charges apply when using an EBS Optimized instance.
     *
     * If this property is not specified, "false" will be used.
     * Required: No
     * Type: Boolean
     *
     * Update requires: some interruptions
     * @var bool
     */
    protected $ebsOptimized;
    /**
     * A reference to an AWS::IAM::InstanceProfile type.
     * For detailed information about IAM Roles, see Working with Roles in the AWS Identity and Access Management User
     * Guide.
     *
     * Required: No
     * Type: String
     *
     * @var string
     */
    protected $iamInstanceProfile;
    /**
     * Provides the unique ID of the Amazon Machine Image (AMI) that was assigned during registration.
     *
     * Required: Yes
     * Type: String
     * Update requires: replacement
     *
     * @var string
     */
    protected $imageId;
    /**
     * The instance type. For example, "m1.small".
     *
     * Required: Yes
     * Type: String
     *
     * Update requires: some interruptions (EBS-backed AMIs)
     * Update requires: replacement (instance-store backed AMIs)
     *
     * @var string
     */
    protected $instanceType;
    /**
     *
     * The kernel ID
     *
     * Required: No
     * Type: String
     *
     * Update requires: some interruptions (EBS-backed AMIs)
     * Update requires: replacement (instance-store backed AMIs)
     *
     * @var string
     */
    protected $kernelId;
    /**
     * Provides the name of the EC2 key pair.
     *
     * Required: No
     * Type: String
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $keyName;
    /**
     * Specifies whether monitoring is enabled for the instance.
     *
     * Required: No
     * Type: Boolean
     *
     * Update requires: no interruption
     *
     * @var bool
     */
    protected $monitoring;
    /**
     * The ARNs of AWS::EC2::NetworkInterface objects to associate with this instance.
     * You can use Ref to get the necessary resource names by specifying the logical IDs of the network interfaces in
     * your template. For examples, see Elastic Network Interface (ENI) Template Snippets.
     *
     * Required: No
     * Type: a list of strings
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $networkInterfaces;
    /**
     * The name of an existing placement group that you want to launch the instance into (for cluster instances).
     *
     * Required: No
     * Type: String
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $placementGroupName;
    /**
     * The private IP address for this instance.
     * If you're using an Amazon Virtual Private Cloud (VPC), you can optionally use this parameter to assign the
     * instance a specific available IP address from the subnet (e.g., 10.0.0.25). By default, Amazon VPC selects an IP
     * address from the subnet for the instance.
     *
     * Required: No
     * Type: String
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $privateIpAddress;
    /**
     * The ID of the RAM disk to select. Some kernels require additional drivers at launch. Check the kernel
     * requirements for information about whether you need to specify a RAM disk. To find kernel requirements, refer
     * to the AWS Resource Center and search for the kernel ID.
     *
     * Required: No
     * Type: String
     *
     * Update requires: some interruptions (EBS-backed AMIs)
     * Update requires: replacement (instance-store backed AMIs)
     *
     * @var string
     */
    protected $ramdiskId;
    /**
     * A list that contains the security group IDs for VPC security groups to assign to the Amazon EC2 instance.
     *
     * Required: Conditional. Required for VPC security groups.
     * Type: a list of strings
     *
     * Update requires: no interruption
     *
     * @var array(string)
     */
    protected $securityGroupIds;
    /**
     * Valid only for EC2 security groups. A list that contains the EC2 security groups to assign to the Amazon EC2
     * instance. The list can contain both the name of existing EC2 security groups or references to
     * AWS::EC2::SecurityGroup resources created in the template.
     *
     * Required: No
     * Type: a list of strings
     * Update requires: replacement.
     *
     * @var array(string)
     */
    protected $securityGroups;
    /**
     * Controls whether source/destination checking is enabled on the instance. Also determines if an instance in a
     * VPC will perform network address translation (NAT).
     * A value of "true" means that source/destination checking is enabled, and a value of "false" means that checking
     * is disabled. For the instance to perform NAT, the value must be "false". For more information, go to NAT
     * Instances in the Amazon Virtual Private Cloud User Guide.
     *
     * Required: No
     * Type: Boolean
     *
     * Update requires: no interruption
     *
     * @var bool
     */
    protected $sourceDestCheck;
    /**
     * If you're using Amazon Virtual Private Cloud, this specifies the ID of the subnet that you want to launch the
     * instance into.
     *
     * Required: No
     * Type: String
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $subnetId;
    /**
     * The tags that you want to attach to the instance.
     *
     * Required: No
     * Type: List of EC2 Tags
     *
     * Update requires: no interruption
     *
     * @var array(Ec2\Tags)
     */
    protected $tags;
    /**
     * The tenancy of the instance that you want to launch. This value can be either "default" or "dedicated". An
     * instance that has a tenancy value of "dedicated" runs on single-tenant hardware and can be launched only into a
     * VPC. For more information, see Using EC2 Dedicated Instances Within Your VPC in the Amazon Virtual Private Cloud
     * User Guide.
     *
     * Required: No
     * Type: String
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $tenancy;
    /**
     * Base64-encoded MIME user data that is made available to the instances.
     *
     * Required: No
     * Type: String
     *
     * Update requires: some interruptions (EBS-backed AMIs)
     * Update requires: replacement (instance-store backed AMIs)
     *
     * @var string
     */
    protected $userData;
    /**
     * The EBS volumes to attach to the instance.
     *
     * Required: No
     * Type: List of EC2 MountPoints.
     *
     * Update requires: You must unmount the filesystem that you want to update.
     *
     * @var array(Ec2\MountPoint)
     */
    protected $volumes;

}

