<?php

namespace ArmadaTests\Resource\AWS\EC2;

class SecurityGroupTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Description of the security group.
     *
     * Type: String
     * Required: Yes
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $GroupDescription;
    /**
     * A list of EC2 security group egress rules.
     *
     * Type: EC2 Security Group Rule Property Type
     * Required: No
     *
     * Update requires: no interruption
     *
     * @var SecurityGroupEgress
     */
    protected $SecurityGroupEgress;
    /**
     * A list of EC2 security group ingress rules.
     *
     * Type: EC2 Security Group Rule Property Type
     *
     * Required: No
     *
     * Update requires: no interruption
     *
     * @var SecurityGroupIngress
     */
    protected $SecurityGroupIngress;
    /**
     * The physical ID of the VPC. Can be obtained by using a reference to an AWS::EC2::VPC, such as: { "Ref" : "myVPC" }.
     *
     * For more information about using the Ref function, see Ref.
     * Type: String
     * Required: Yes, for VPC security groups
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $VpcId;
}

