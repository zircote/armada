<?php

namespace ArmadaTests\Resource\AWS\EC2;

/**
 *
 * The AWS::EC2::EIPAssociation resource associates an Elastic IP address with an Amazon EC2 instance. The Elastic IP
 * address can be an existing Elastic IP address or an Elastic IP address allocated through an AWS::EC2::EIP resource.
 *
 */
class EIPAssociationTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Allocation ID for the VPC Elastic IP address you want to associate with an Amazon EC2 instance in your VPC.
     *
     * Required: Conditional. This property must not be specified if the EIP property is specified.
     * Type: String
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $allocationId;
    /**
     * Elastic IP address that you want to associate with the Amazon EC2 instance specified by the InstanceId property.
     * You can specify an existing Elastic IP address or a reference to an Elastic IP address allocated with a
     * AWS::EC2::EIP resource.
     *
     * Required: Conditional. This property must not be specified if the AllocationId property is specified.
     * Type: String
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $eip;
    /**
     * Instance ID of the Amazon EC2 instance that you want to associate with the Elastic IP address specified by the
     * EIP property.
     *
     * Required: No
     * Type: String
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $instanceId;
    /**
     * The ID of the network interface to associate with the Elastic IP address (VPC only).
     *
     * Required: No
     * Type: String
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $networkInterfaceId;

}

