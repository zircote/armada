<?php

namespace ArmadaTests\Resource\AWS\CloudWatch;

/**
 *
 * The AWS::CloudWatch::Alarm type creates an Amazon CloudWatch alarm.
 * This type supports updates. For more information about updating this resource, see PutMetricAlarm. For more
 * information about updating stacks, see Updating AWS CloudFormation Stacks.
 * When you specify an AWS::CloudWatch::Alarm type as an argument to the Ref function, AWS CloudFormation returns the
 * value of the AlarmName.
 *
 */
class AlarmTest extends \PHPUnit_Framework_TestCase
{

}
