<?php
namespace ArmadaTests\Resource\CloudFormation;

/**
 * @package     Armada
 * @category    Resource
 * @subcategory CloudFormation
 */
use Armada\Resource\AbstractResource;

/**
 * Use the AWS::CloudFormation::Authentication type to specify authentication credentials for files or sources that you
 * specify with the AWS::CloudFormation::Init type.
 *
 * To include authentication information for a file or source that you specify with AWS::CloudFormation::Init, use the
 * uris property if the source is a URI or the buckets property if the source is an Amazon S3 bucket. For more
 * information about files, see Files. For more information about sources, see Sources.
 *
 * You can also specify authentication information for files directly in the AWS::CloudFormation::Init resource. The
 * files key of the resource contains a property named authentication. You can use the authentication property to
 * associate authentication information defined in an AWS::CloudFormation::Authentication resource directly with a file.
 *
 * For files, AWS CloudFormation looks for authentication information in the following order:
 *      - The authentication property of the AWS::CloudFormation::Init files key.
 *      - The uris or buckets property of the AWS::CloudFormation::Authentication resource.
 *
 * For sources, AWS CloudFormation looks for authentication information in the uris or buckets property of the
 * AWS::CloudFormation::Authentication resource.
 *
 * @see http://docs.amazonwebservices.com/AWSCloudFormation/latest/UserGuide/aws-resource-authentication.html
 */
class AuthenticationTest extends \PHPUnit_Framework_TestCase
{

}
