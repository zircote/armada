<?php
namespace ArmadaTests\Resource\CloudFormation;

/**
 * @package     Armada
 * @category    Resource
 * @subcategory CloudFormation
 */
use Armada\Resource\AbstractResource;

/**
 * Custom resources are special AWS CloudFormation resources that provide a way for a template developer to include
 * resources in an AWS CloudFormation stack that are provided by a source other than Amazon Web Services. The custom
 * resource provider can be either a template developer or a separate third-party resource provider.
 *
 * In a template, a custom resource is represented by AWS::CloudFormation::CustomResource.
 *
 * @package
 * @category
 * @subcategory
 */
class CustomResourceTest extends \PHPUnit_Framework_TestCase
{

}
