<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 *
 * You can use the users key to create Linux/UNIX users on the EC2 instance. The following table lists the supported
 * keys.
 *
 * <b>Users are created as non-interactive system users with a shell of /sbin/nologin. This is by design and cannot be modified.</b>
 *
 * <code>
 * "users" : {
 *     "myUser" : {
 *         "groups" : ["groupOne", "groupTwo"],
 *         "uid" : "50",
 *         "homeDir" : "/tmp"
 *     }
 * }
 * </code>
 */
class UsersTest extends \PHPUnit_Framework_TestCase
{
    /**
     * A user ID. The creation process fails if the user name exists with a different user ID. If the user ID is already
     * assigned to an existing user the operating system may reject the creation request.
     * @var
     */
    protected $uid;
    /**
     * A list of group names. The user will be added to each group in the list.
     * @var
     */
    protected $groups;
    /**
     * The user's home directory.
     * @var
     */
    protected $homeDir;
}
