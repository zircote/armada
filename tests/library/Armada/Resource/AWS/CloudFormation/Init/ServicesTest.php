<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 *
 * You can use the services key to define which services should be enabled or disabled when the instance is launched.
 * The services key also allows you to specify dependencies on sources, packages and files so that if a restart is
 * needed due to files being installed, cfn-init will take care of the service restart. For example, if you download
 * the Apache HTTP Server package, the package installation will automatically start the Apache HTTP Server during the
 * stack creation process. However, if the Apache HTTP Server configuration is updated later in the stack creation
 * process, the update won't take effect unless the Apache server is restarted. You can use the services key to ensure
 * that the Apache HTTP service is restarted.
 *
 * <code>
 * "services" : {
 *   "sysvinit" : {
 *     "nginx" : {
 *       "enabled" : "true",
 *       "ensureRunning" : "true",
 *       "files" : ["/etc/nginx/nginx.conf"],
 *       "sources" : ["/var/www/html"]
 *     },
 *     "php-fastcgi" : {
 *       "enabled" : "true",
 *       "ensureRunning" : "true",
 *       "packages" : { "yum" : ["php", "spawn-fcgi"] }
 *     },
 *     "sendmail" : {
 *       "enabled" : "false",
 *       "ensureRunning" : "false"
 *     }
 *   }
 * }
 * </code>
 */
class ServicesTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * Set to true to ensure that the service is running after cfn-init finishes.
     * Set to false to ensure that the service is not running after cfn-init finishes.
     * Omit this key to make no changes to the service state.
     *
     * @var
     */
    protected $ensureRunning;
    /**
     * Set to true to ensure that the service will be started automatically upon boot.
     * Set to false to ensure that the service will not be started automatically upon boot.
     * Omit this key to make no changes to this property.
     *
     * @var
     */
    protected $enabled;
    /**
     * A list of files. If cfn-init changes one directly via the files block, this service will be restarted
     * @var
     */
    protected $files;
    /**
     * A list of directories. If cfn-init expands an archive into one of these directories, this service will be
     * restarted.
     *
     * @var
     */
    protected $sources;
    /**
     * A map of package manager to list of package names. If cfn-init installs or updates one of these packages, this
     * service will be restarted.
     *
     * @var
     */
    protected $packages;
    /**
     * A list of command names. If cfn-init runs the specified command, this service will be restarted.
     * @var
     */
    protected $commands;
}
