<?php
namespace Armada\Mapping;

/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class Map
{
    /**
     * @var string
     */
    protected $name;
    /**
     * @var array
     */
    protected $map = array();

    /**
     * @param $name
     * @param array $map
     * @throws \Exception
     */
    public function __construct($name, $map = array())
    {
        if(!preg_match('/^[a-zA-Z]{1}[a-zA-Z0-9]+$/', $name)){
            throw new \Exception('invalid mapping name /^[a-zA-Z]{1}[a-zA-Z0-9]+$/ expected');
        }
        $this->name = $name;
        foreach ($map as $mapKey => $item) {
            if(is_array($item)){
                list($index, $value) = $item;
                $this->pushItem($mapKey, $index, $value);
            }
        }
    }

    /**
     * @param string $mapKey
     * @param string $index
     * @param mixed  $value
     */
    public function pushItem($mapKey, $index, $value)
    {
        $this->map[$mapKey] = array($index => $value);
    }
    public function __toString()
    {
        $result = new \stdClass();
        $result->{$this->name} = $this->map;
        return json_encode($result);
    }
}
