<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class PrimaryKey
{
    /**
     * The name of the attribute that will serve as the primary key for this table. Primary key element names can be
     * 1 – 255 characters long and have no character restrictions.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $attributeName;
    /**
     * The type of this attribute. This must be either "S" for string data, or "N" for numeric data.
     *
     * Required: Yes
     * Type: String
     * Note: For detailed information about the limits of primary key values in Amazon DynamoDB, see Limits in Amazon
     * DynamoDB in the Amazon DynamoDB Developer Guide.
     *
     * @var
     */
    protected $attributeType;

}
