<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class ProvisionedThroughput
{
    /**
     * Sets the desired minimum number of consistent reads of items (of up to 1KB in size) per second for the specified
     * table before Amazon DynamoDB balances the load.
     *
     * Required: Yes
     * Type: Number
     *
     * @var
     */
    protected $readCapacityUnits;
    /**
     * Sets the desired minimum number of consistent writes of items (of up to 1KB in size) per second for the specified
     * table before Amazon DynamoDB balances the load.
     *
     * Note: For detailed information about the limits of provisioned throughput values in Amazon DynamoDB, see Limits
     * in Amazon DynamoDB in the Amazon DynamoDB Developer Guide.
     *
     * @var
     */
    protected $writeCapacityUnits;
}
