<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class NetworkInterfaceAttachment
{
    /**
     * The ID of the network interface attachment.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $attachmentID;
    /**
     * The ID of the instance attached to the network interface.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $instanceID;
}
