<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class NetworkInterfaceGroup
{
    /**
     * ID of the security group.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $key;
    /**
     * Name of the security group.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $value;
}
