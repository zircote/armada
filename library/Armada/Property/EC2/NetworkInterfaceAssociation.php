<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class NetworkInterfaceAssociation
{
    /**
     * The ID of the network interface attachment.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $attachmentID;
    /**
     * The ID of the instance attached to the network interface.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $instanceID;
    /**
     * The address of the Elastic IP address bound to the network interface.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $publicIp;
    /**
     * The ID of the Elastic IP address owner.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $ipOwnerId;
}
