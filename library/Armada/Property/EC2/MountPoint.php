<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class MountPoint
{
    /**
     * How the device is exposed to the instance (such as /dev/sdh, or xvdh).
     *
     * Type: String
     * Required: Yes
     *
     * @var
     */
    protected $device;
    /**
     * The ID of the Amazon EBS volume. The volume and instance must be within the same Availability Zone and the
     * instance must be running.
     *
     * Type: String
     * Required: Yes
     *
     * @var
     */
    protected $volumeId;
}
