<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class PortRange
{
    /**
     * The first port in the range.
     * Condition: Required if specifying 6 (TCP) or 17 (UDP) for the CreateNetworkAclEntry protocol parameter.
     *
     * Type: Integer
     * Required: Conditional
     *
     * @var
     */
    protected $from;
    /**
     * The last port in the range.
     * Condition: Required if specifying 6 (TCP) or 17 (UDP) for the CreateNetworkAclEntry protocol parameter.
     *
     * Type: Integer
     * Required: Conditional
     *
     * @var
     */
    protected $to;
}
