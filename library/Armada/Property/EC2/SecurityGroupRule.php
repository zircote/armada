<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class SecurityGroupRule
{
    /**
     * IP protocol name or number. For valid values, go to the IpProtocol parameter in AuthorizeSecurityGroupIngress
     * Type: String
     * Required: Yes
     *
     * @var
     */
    protected $ppProtocol;
    /**
     * If you specify SourceSecurityGroupName, do not specify CidrIp.
     *
     * Type: String
     * Required: Conditional
     *
     * @var
     */
    protected $cidrIp;
    /**
     * Specifies the name of the Amazon EC2 Security Group to allow access or uses the Ref intrinsic function to refer
     * to the logical name of a security group defined in the same template.
     * If you specify CidrIp, do not specify SourceSecurityGroupName.
     * Type: String
     * Required: Conditional
     *
     * @var
     */
    protected $sourceSecurityGroupName;
    /**
     * For VPC security groups only. Specifies the ID of the Amazon EC2 Security Group to allow access or uses the Ref
     * intrinsic function to refer to the logical ID of a security group defined in the same template.
     *
     * Condition: If you specify CidrIp, do not specify SourceSecurityGroupName.
     * Type: String
     * Required: Conditional
     *
     * @var
     */
    protected $sourceSecurityGroupId;
    /**
     * Specifies the AWS Account ID of the owner of the Amazon EC2 Security Group specified in the
     * SourceSecurityGroupName property.
     * If you specify SourceSecurityGroupName and that security group is owned by a different account than the account
     * creating the stack, you must specify the SourceSecurityGroupOwnerId; otherwise, this property is optional.
     * Type: String
     * Required: Conditional
     *
     * @var
     */
    protected $sourceSecurityGroupOwnerId;
    /**
     * Start of port range for the TCP and UDP protocols, or an ICMP type number. An ICMP type number of -1 indicates a
     * wildcard (i.e., any ICMP type number).
     * Type: String
     * Required: Yes
     *
     * @var
     */
    protected $fromPort;
    /**
     * End of port range for the TCP and UDP protocols, or an ICMP code. An ICMP code of -1 indicates a wildcard (i.e.,
     * any ICMP code).
     * Type: String
     * Required: Yes
     *
     * @var
     */
    protected $toPort;
}
