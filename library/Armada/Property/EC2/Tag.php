<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class Tag
{
    /**
     * The key term for this item.
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $key;
    /**
     * A value associated with the key term.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $value;


}
