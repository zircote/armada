<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class ICMP
{
    /**
     * The Internet Control Message Protocol (ICMP) code. You can use -1 to specify all ICMP codes for the given ICMP type.
     * Condition: Required if specifying 1 (ICMP) for the CreateNetworkAclEntry protocol parameter.
     *
     * Type: Integer
     * Required: Conditional
     *
     * @var
     */
    protected $code;
    /**
     * The Internet Control Message Protocol (ICMP) type. You can use -1 to specify all ICMP types.
     * Condition: Required if specifying 1 (ICMP) for the CreateNetworkAclEntry protocol parameter.
     *
     * Type: Integer
     * Required: Conditional
     *
     * @var
     */
    protected $type;
}
