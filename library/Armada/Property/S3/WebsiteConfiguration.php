<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class WebsiteConfiguration
{
    /**
     * You can specify this property using the GetAtt intrinsic function to get the CanonicalHostedZoneNameID property
     * for the LoadBalancer resource you want.
     * The hosted zone name ID of the Load Balancer that is the target of the alias.
     *
     * Type: String
     * Required: Yes

     * @var
     */
    protected $hostedZoneId;
    /**
     * You can specify this property using the GetAtt intrinsic function to get the CanonicalHostedZoneName property for
     * the LoadBalancer resource you want.
     * The DNS name of the Load Balancer that is the target of the alias.
     *
     * Type: String
     * Required: Yes
     *
     * @var
     */
    protected $dnsName;
}
