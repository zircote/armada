<?php
namespace Armada\Property\AutoScaling;

/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class BlockDeviceMapping
{
    /**
     * The name of the device within Amazon EC2.
     * Type: String
     * Required: Yes
     *
     * @var string
     */
    protected $deviceName;
    /**
     * The name of the virtual device. The name must be in the form ephemeralX where X is a number starting from 0, for
     * example, ephemeral0. If you specify the VirtualName property, do not specify the Ebs property.
     * Type: String
     * Required: Conditional
     *
     * @var string
     */
    protected $virtualName;
    /**
     * The information for the Elastic Block Store volume. If you specify the Ebs property, do not specify the
     * VirtualName property.
     * Type: Block Device Template type
     * Required: Conditional
     *
     * @var
     */
    protected $ebs;
}
