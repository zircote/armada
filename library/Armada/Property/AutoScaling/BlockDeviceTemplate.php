<?php
namespace Armada\Property\AutoScaling;

/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class BlockDeviceTemplate
{
    /**
     * The Snapshot ID of the volume to use.
     * Type: String
     * Required: Conditional
     *
     * If you specify SnapshotId, do not specify VolumeSize.
     *
     * @var string
     */
    protected $snapshotId;
    /**
     * If you specify VolumeSize, do not specify SnapshotId.
     *
     * The volume size, in GigiBytes.
     *
     * Type: String
     * Required: Conditional
     *
     * @var string
     */
    protected $volumeSize;
}
