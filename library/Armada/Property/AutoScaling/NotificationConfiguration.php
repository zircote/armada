<?php
namespace Armada\Property\AutoScaling;

/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class NotificationConfiguration
{

    /**
     * The Amazon Resource Name (ARN) of the Amazon Simple Notification Service (SNS) topic.
     *
     * @var string
     */
    protected $topicARN;
    /**
     * The type of events that will trigger the notification. A list of events that will trigger the notification,
     * which can include any or all of the following: autoscaling: EC2_INSTANCE_LAUNCH,
     * autoscaling:EC2_INSTANCE_LAUNCH_ERROR, autoscaling:EC2_INSTANCE_TERMINATE,
     * autoscaling:EC2_INSTANCE_TERMINATE_ERROR, and autoscaling:TEST_NOTIFICATION. For more information about event
     * types,
     *
     * @see http://docs.amazonwebservices.com/AutoScaling/latest/APIReference/API_DescribeAutoScalingNotificationTypes.html
     *
     * @var array[string]
     */
    protected $notificationTypes = array();
}
