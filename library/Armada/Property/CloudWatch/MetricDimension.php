<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class MetricDimension
{
    /**
     * Name of the dimension.
     *
     * Type: String
     * Required: Yes
     *
     * @var
     */
    protected $name;
    /**
     * The value representing the dimension measurement.
     *
     * Type: String
     * Required: Yes
     *
     * @var
     */
    protected $value;
}
