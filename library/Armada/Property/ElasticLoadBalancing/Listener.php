<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class Listener
{
    /**
     * Specifies the TCP port on which the instance server is listening. This property cannot be modified for the life
     * of the load balancer.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $instancePort;
    /**
     * Specifies the protocol to use for routing traffic to back-end instances—HTTP, HTTPS, TCP, or SSL. This property
     * cannot be modified for the life of the load balancer.
     *
     * Required: Yes
     * Type: String
     *
     * Note: If the front-end protocol is HTTP or HTTPS, InstanceProtocol has to be at the same protocol layer, i.e.,
     * HTTP or HTTPS. Likewise, if the front-end protocol is TCP or SSL, InstanceProtocol has to be TCP or SSL.
     * If there is another listener with the same InstancePort whose InstanceProtocol is secure, i.e., HTTPS or SSL,
     * the listener's InstanceProtocol has to be secure, i.e., HTTPS or SSL. If there is another listener with the same
     * InstancePort whose InstanceProtocol is HTTP or TCP, the listener's InstanceProtocol must be either HTTP or TCP.
     *
     * @var
     */
    protected $instanceProtocol;
    /**
     * Specifies the external load balancer port number. This property cannot be modified for the life of the load
     * balancer.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $loadBalancerPort;
    /**
     * A list of ElasticLoadBalancing policy names to associate with the listener.
     *
     * Required: No
     * Type: a list of strings
     *
     * @var
     */
    protected $policyNames;
    /**
     * Specifies the load balancer transport protocol to use for routing — TCP or HTTP. This property cannot be
     * modified for the life of the load balancer.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $protocol;
    /**
     * The ARN of the SSL certificate to use. For more information about SSL certificates, see Managing Server
     * Certificates in the AWS Identity and Access Management documentation.
     *
     * Required: No
     * Type: String
     *
     * @var
     */
    protected $sslCertificateId;
}
