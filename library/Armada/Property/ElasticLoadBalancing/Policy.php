<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class Policy
{
    /**
     * A list of arbitrary attributes for this policy.
     *
     * Required: No
     * Type: List of JSON name-value pairs.
     *
     * @var
     */
    protected $attributes;
    /**
     * A list of instance ports for the policy. These are the ports associated with the back-end server.
     *
     * Required: No
     * Type: String
     *
     * @var
     */
    protected $instancePorts;
    /**
     * A list of external load balancer ports for the policy.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $loadBalancerPorts;
    /**
     * A name for this policy that is unique to the load balancer.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $policyName;
    /**
     * The name of the policy type for this policy. This must be one of the types reported by the Elastic Load
     * Balancing DescribeLoadBalancerPolicyTypes action.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $policyType;

}
