<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class HealthCheck
{
    /**
     * Specifies the number of consecutive health probe successes required before moving the instance to the Healthy
     * state.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $healthyThreshold;
    /**
     * Specifies the approximate interval, in seconds, between health checks of an individual instance.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $interval;
    /**
     * Specifies the instance being checked. The protocol is either TCP or HTTP. The range of valid ports is
     * 1 through 65535.
     *
     * Required: Yes
     * Type: String
     *
     * Note: TCP is the default, specified as a TCP: port pair—for example, "TCP:5000". In this case a healthcheck
     * simply attempts to open a TCP connection to the instance on the specified port. Failure to connect within the
     * configured timeout is considered unhealthy.
     * For HTTP, the situation is different. HTTP is specified as a HTTP:port;/;PathToPing; grouping—for example,
     * "HTTP:80/weather/us/wa/seattle". In this case, a HTTP GET request is issued to the instance on the given port
     * and path. Any answer other than "200 OK" within the timeout period is considered unhealthy.
     * The total length of the HTTP ping target needs to be 1024 16-bit Unicode characters or fewer.
     *
     * @var
     */
    protected $target;
    /**
     * Specifies the amount of time, in seconds, during which no response means a failed health probe. This value must
     * be less than the value for Interval.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $timeout;
    /**
     * Specifies the number of consecutive health probe failures required before moving the instance to the Unhealthy
     * state.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $unhealthyThreshold;
}
