<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class AppCookieStickinessPolicy
{
    /**
     * Name of the application cookie used for stickiness.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $cookieName;
    /**
     * The name of the policy being created. The name must be unique within the set of policies for this Load Balancer.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $policyName;


}
