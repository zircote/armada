<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class LBCookieStickinessPolicy
{
    /**
     * The time period, in seconds, after which the cookie should be considered stale. If this parameter isn't
     * specified, the sticky session will last for the duration of the browser session.
     *
     * Required: No
     * Type: String
     *
     * @var
     */
    protected $cookieExpirationPeriod;
    /**
     * The name of the policy being created. The name must be unique within the set of policies for this load balancer.
     * @var
     */
    protected $policyName;
}
