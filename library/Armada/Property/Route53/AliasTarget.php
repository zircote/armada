<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class AliasTarget
{
    /**
     * The IP range to authorize.
     * For an overview of CIDR ranges, go to the Wikipedia Tutorial.
     *
     * Type: String
     * Required: No
     *
     * Update requires: replacement
     *
     * @var
     */
    protected $CIDRIP;
    /**
     * Id of the VPC or EC2 Security Group to authorize.
     * For VPC DB Security Groups, use EC2SecurityGroupId. For EC2 Security Groups, use EC2SecurityGroupOwnerId and
     * either EC2SecurityGroupName or EC2SecurityGroupId.
     *
     * Type: String
     * Required: No
     *
     * Update requires: replacement
     *
     * @var
     */
    protected $ec2SecurityGroupId;
    /**
     * Name of the EC2 Security Group to authorize.
     * For VPC DB Security Groups, use EC2SecurityGroupId. For EC2 Security Groups, use EC2SecurityGroupOwnerId and
     * either EC2SecurityGroupName or EC2SecurityGroupId.
     *
     * Type: String
     * Required: No
     *
     * Update requires: replacement
     *
     * @var
     */
    protected $ec2SecurityGroupName;
    /**
     * AWS Account Number of the owner of the EC2 Security Group specified in the EC2SecurityGroupName parameter. The
     * AWS Access Key ID is not an acceptable value.
     *
     * For VPC DB Security Groups, use EC2SecurityGroupId. For EC2 Security Groups, use EC2SecurityGroupOwnerId and
     * either EC2SecurityGroupName or EC2SecurityGroupId.
     *
     * Type: String
     * Required: No
     *
     * Update requires: replacement
     *
     * @var
     */
    protected $ec2SecurityGroupOwnerId;
}
