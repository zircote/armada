<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 * Describes the Amazon CloudFront cache behavior when the requested URL matches a pattern. This is an embedded
 * property of the DistributionConfig type.
 *
 */
class CacheBehavior
{
    /**
     * The ID value of the origin to which you want CloudFront to route requests when a request matches the PathPattern.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $targetOriginId;
    /**
     * Specifies how CloudFront handles query strings.
     *
     * Required: Yes
     * Type: ForwardedValues type
     *
     * @var
     */
    protected $forwardedValues;
    /**
     * A list of AWS accounts that you want to allow to create signed URLs for private content.
     *
     * Required: No
     * Type: list of Strings
     *
     * @var
     */
    protected $trustedSigners;
    /**
     * Specifies the protocol that users can use to access the files in the origin specified by TargetOriginId when a
     * request matches the PathPattern.
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $viewerProtocolPolicy;
    /**
     * The minimum amount of time that you want objects to stay in the cache before CloudFront queries your origin to
     * see whether the object has been updated.
     *
     * Required: No
     * Type: String
     *
     * @var
     */
    protected $minTTL;
    /**
     * The pattern for which you want this cache behavior to apply (ex. "images/*.jpg").
     * When CloudFront receives an end-user request, the requested path is compared with path patterns in the order in
     * which cache behaviors are listed in the stack specification for the distribution.
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $pathPattern;
}
