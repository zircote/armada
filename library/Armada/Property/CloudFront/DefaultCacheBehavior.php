<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 *
 * <code>
 * {
 *    "TargetOriginId" : String,
 *    "ForwardedValues" : ForwardedValues,
 *    "TrustedSigners" : [ String, ... ],
 *    "ViewerProtocolPolicy" : String,
 *    "MinTTL" : String
 * }
 * </code>
 */
class DefaultCacheBehavior
{
    /**
     * The value of ID for the origin that you want CloudFront to route requests to when the default cache behavior is
     * applicable to a request.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $targetOriginId;
    /**
     * Specifies how CloudFront handles query strings.
     *
     * Required: Yes
     * Type: ForwardedValues type
     *
     * @var
     */
    protected $forwardedValues;
    /**
     * A list of AWS accounts that you want to allow to create signed URLs for private content.
     *
     * Required: No
     * Type: list of Strings
     *
     * @var
     */
    protected $trustedSigners;
    /**
     * Specifies the protocol that users can use to access the files in the origin specified by TargetOriginId when the
     * default cache behavior is applicable to a request.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $viewerProtocolPolicy;
    /**
     * The minimum amount of time that you want objects to stay in the cache before CloudFront queries your origin to
     * see whether the object has been updated.
     *
     * Required: No
     * Type: String
     *
     * @var
     */
    protected $minTTL;

}
