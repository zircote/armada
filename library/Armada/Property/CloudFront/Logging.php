<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class Logging
{
    /**
     * The Amazon S3 bucket to store the access logs in.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $bucket;
    /**
     * An optional string that, if defined, will be used as a prefix for the access log file names for this distribution.
     *
     * Required: No
     * Type: String
     *
     * @var
     */
    protected $prefix;
}
