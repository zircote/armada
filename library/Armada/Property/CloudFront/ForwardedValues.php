<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class ForwardedValues
{
    /**
     * Indicates whether you want CloudFront to forward query strings to the origin that is associated with this cache
     * behavior. If so, specify "true"; if not, specify "false".
     *
     * Required: Yes
     * Type: Boolean
     *
     * @var bool
     */
    protected $queryString;
}
