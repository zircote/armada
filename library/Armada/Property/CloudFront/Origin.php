<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class Origin
{
    /**
     * The DNS name of the Amazon S3 bucket or the HTTP server from which you want CloudFront to get objects for this
     * origin.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $domainName;
    /**
     * An identifier for the origin. The value of Id must be unique within the distribution.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $id;
    /**
     * Origin information to specify an Amazon S3 origin.
     *
     * Required: Conditional. You cannot use S3Origin and CustomOrigin in the same distribution, but you must specify
     * one or the other.
     * Type: S3Origin type
     *
     * @var
     */
    protected $s3OriginConfig;
    /**
     * Origin information to specify a custom origin.
     *
     * Required: Conditional. You cannot use CustomOrigin and S3 Origin in the same distribution, but you must specify
     * one or the other.
     * Type: CustomOrigin type
     *
     * @var
     */
    protected $customOriginConfig;
}
