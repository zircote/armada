<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class S3Origin
{
    /**
     * The CloudFront origin access identity to associate with the origin. This is used to configure the origin so that
     * end users can only access objects in an Amazon S3 bucket through CloudFront.
     *
     * Required: No
     * Type: String
     *
     * @var
     */
    protected $originAccessIdentity;
}
