<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class CustomOrigin
{
    /**
     * CNAMEs (alternate domain names), if any, for the distribution.
     *
     * Required: No
     * Type: List of Strings
     *
     * @var
     */
    protected $aliases;
    /**
     * A list of CacheBehaviors for the distribution.
     *
     * Required: No
     * Type: String
     *
     * Update requires: no interruption
     *
     * @var
     */
    protected $cacheBehaviors;
    /**
     * Any comments you want to include about the distribution.
     *
     * Required: No
     * Type: String
     *
     * Update requires: no interruption
     *
     * @var
     */
    protected $comment;
    /**
     * The default cache behavior that is triggered if you do not specify a CacheBehavior element, or if files don't
     * match any of the values of PathPattern in CacheBehavior elements.
     *
     * Required: Yes
     * Type: DefaultCacheBehavior type
     *
     * Update requires: no interruption
     *
     * @var
     */
    protected $defaultCacheBehavior;
    /**
     * The object (ex. "index.html") that you want CloudFront to request from your origin when the root URL for your
     * distribution (ex. "http://example.com/") is requested.
     * Note: Specifying a default root object avoids exposing the contents of your distribution.
     *
     * Required: No
     * Type: String
     *
     * @var
     */
    protected $defaultRootObject;
    /**
     * Controls whether the distribution is enabled to accept end user requests for content.
     *
     * Required: Yes
     * Type: Boolean
     *
     * @var
     */
    protected $enabled;
    /**
     * Controls whether access logs are written for the distribution. To turn on access logs, include this property.
     *
     * Required: No
     * Type: Logging type
     *
     * @var
     */
    protected $logging;
    /**
     * A list of origins for this CloudFront distribution. For each origin, you can specify whether it is an S3 or
     * custom origin.
     *
     * Required: Yes
     * Type: list of Origins.
     *
     * @var
     */
    protected $origins;
}
