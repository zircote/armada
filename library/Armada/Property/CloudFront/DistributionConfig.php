<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class DistributionConfig
{
    /**
     * The HTTP port the custom origin listens on.
     *
     * Required: No
     * Type: String
     *
     * @var
     */
    protected $httpPort;
    /**
     * The HTTPS port the custom origin listens on.
     *
     * Required: No
     * Type: String
     *
     * @var
     */
    protected $httpsPort;
    /**
     * The origin protocol policy to apply to your origin.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $originProtocolPolicy;
}
