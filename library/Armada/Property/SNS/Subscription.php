<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class Subscription
{

    /**
     * The name of the index document. For more information, see Index Document Support.
     *
     * Type: String
     * Required: No
     *
     * @var
     */
    protected $indexDocument;
    /**
     * The name of the error document. For more information, see Custom Error Document Support.
     *
     * Type: String
     * Required:No
     *
     * @var
     */
    protected $errorDocument;
}
