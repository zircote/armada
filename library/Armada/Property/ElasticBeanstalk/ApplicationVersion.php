<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class ApplicationVersion
{
    /**
     * A description for this ApplicationVersion.
     * Required: No
     * Type: String
     * @var
     */
    protected $description;
    /**
     * The location where the source bundle is located for this version.
     * Required: No
     * Type: Source Bundle
     *
     * @var
     */
    protected $sourceBundle;
    /**
     * A label that uniquely identifies the version of the associated application.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $versionLabel;
}
