<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class SourceBundle
{
    /**
     * The Amazon S3 bucket where the data is located.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $s3Bucket;
    /**
     * The Amazon S3 key where the data is located.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $s3Key;
}
