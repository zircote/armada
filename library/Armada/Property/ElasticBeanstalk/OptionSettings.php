<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
class OptionSettings
{
    /**
     * A unique namespace identifying the option's associated AWS resource.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $namespace;
    /**
     * The name of the configuration option.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $optionName;
    /**
     * The value of the setting.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $value;
}
