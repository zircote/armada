<?php

namespace Armada\Resource\AWS\Route53;

use Armada\Resource\AbstractResource;

class RecordSetGroup extends AbstractResource
{
    const NAME = 'AWS::Route53::RecordSetGroup';
    /**
     *The ID of the hosted zone.
     * Required: Conditional: You must specify either the HostedZoneName or HostedZoneId, but you cannot specify both.
     * Type: String
     * @var string
     */
    protected $HostedZoneId;
    /**
     * The name of the domain for the hosted zone where you want to add the record set.
     *
     * When you create a stack using an AWS::Route53::RecordSet that specifies HostedZoneName, AWS CloudFormation
     * attempts to find a hosted zone whose name matches the HostedZoneName. If AWS CloudFormation cannot find a hosted
     * zone with a matching domain name, or if there is more than one hosted zone with the specified domain name, AWS
     * CloudFormation will not create the stack.
     *
     * If you have multiple hosted zones with the same domain name, you must explicitly specify the hosted zone using
     * HostedZoneId.
     *
     * Required: Conditional. You must specify either the HostedZoneName or HostedZoneId, but you cannot specify both.
     * Type: String
     * @var string
     */
    protected $HostedZoneName;
    /**
     * List of resource record sets to add.
     * Required: Yes
     * Type: list of AWS::Route53::RecordSet
     * @var array(Route53\RecordSet)
     */
    protected $RecordSets = array();
    /**
     *Any comments you want to include about the hosted zone.
     * Required: No
     * Type: String
     *
     * @var string
     */
    protected $Comment;

}

