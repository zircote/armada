<?php

namespace Armada\Resource\AWS\Route53;


use Armada\Resource\AbstractResource;

class RecordSet extends AbstractResource
{
    const NAME = 'AWS::Route53::RecordSet';
    /**
     * Alias resource record sets only: Information about the domain to which you are redirecting traffic.
     * For more information about alias resource record sets, see Creating Alias Resource Record Sets in the Route 53
     * Developer Guide.
     * Note: Currently, Amazon Route 53 supports aliases only for Elastic Load Balancing.
     *
     * Required: Conditional. Required if you are creating an alias resource record set.
     * Type: AliasTarget
     *
     * @var JSON(object)
     */
    protected $aliasTarget;
    /**
     * Any comments you want to include about the hosted zone.
     * Required: No
     * Type: String
     *
     * @var string
     */
    protected $Comment;
    /**
     * The ID of the hosted zone.
     * Required: Conditional. You must specify either the HostedZoneName or HostedZoneId, but you cannot specify both.
     * Type: String
     *
     * @var string
     */
    protected $HostedZoneId;
    /**
     * The name of the domain for the hosted zone where you want to add the record set.
     * When you create a stack using an AWS::Route53::RecordSet that specifies HostedZoneName, AWS CloudFormation
     * attempts to find a hosted zone whose name matches the HostedZoneName. If AWS CloudFormation cannot find a hosted
     * zone with a matching domain name, or if there is more than one hosted zone with the specified domain name, AWS
     * CloudFormation will not create the stack.
     *
     * If you have multiple hosted zones with the same domain name, you must explicitly specify the hosted zone using
     * HostedZoneId.
     *
     * Required: Conditional. You must specify either the HostedZoneName or HostedZoneId, but you cannot specify both.
     * Type: String
     *
     * @var string
     */
    protected $HostedZoneName;
    /**
     * The name of the domain. This must be a fully-specified domain, ending with a period as the last label indication.
     * If you omit the final period, Amazon Route 53 assumes the domain is relative to the root.
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $Name;
    /**
     * Latency resource record sets only: The Amazon EC2 region where the resource that is specified in this resource
     * record set resides. The resource typically is an AWS resource, for example, Amazon EC2 instance or an Elastic
     * Load Balancing load balancer, and is referred to by an IP address or a DNS domain name, depending on the record
     * type.
     *
     * When Route 53 receives a DNS query for a domain name and type for which you have created latency resource record
     * sets, Route 53 selects the latency resource record set that has the lowest latency between the end user and the
     * associated Amazon EC2 region. Route 53 then returns the value that is associated with the selected resource
     * record set.
     *
     * The following restrictions must be followed:
     *     - You can only specify one ResourceRecord per latency resource record set.
     *     - You can only create one latency resource record set for each Amazon EC2 region.
     *     - You are not required to create latency resource record sets for all Amazon EC2 regions. Route 53
     *          will choose the region with the best latency from among the regions for which you create latency resource
     *          record sets.
     *     - You cannot create both weighted and latency resource record sets that have the same values for the Name
     *          and Type elements.
     *     - Valid values by region name:
     *          - Asia Pacific (Tokyo) Region: "ap-northeast-1"
     *          - Asia Pacific (Singapore) Region: "ap-southeast-1"
     *          - Asia Pacific (Sydney) Region: "ap-southeast-2"
     *          - EU (Ireland) Region: "eu-west-1"
     *          - South America (Sao Paulo) Region: "sa-east-1"
     *          - US East (Northern Virginia) Region: "us-east-1"
     *          - US West (Northern California) Region: "us-west-1"
     *          - US West (Oregon) Region: "us-west-2"
     *
     * @var string
     */
    protected $Region;
    /**
     * List of resource records to add. Each record should be in the format appropriate for the record type specified
     * by the Type property. For information about different record types and their record formats,
     *
     * @see Appendix: Domain Name Format in the Route 53 Developer Guide.
     * Required: Conditional. Required if TTL is specified.
     * Type: list of Strings
     *
     * @var array(string)
     */
    protected $ResourceRecords = array();
    /**
     * A unique identifier that differentiates among multiple resource record sets that have the same combination of
     * DNS name and type.
     *
     * Required: Conditional. Required if you are creating a weighted resource record set.
     *
     * For more information about weighted resource record sets, see Setting Up Weighted Resource Record Sets in the
     * Route 53 Developer Guide.
     * Type: String
     * @var string
     */
    protected $SetIdentifier;
    /**
     * The resource record cache time to live (TTL), in seconds.
     *
     * If TTL is specified, ResourceRecords is also required.
     *
     * Required: No
     * Type: String
     * @var string
     */
    protected $TTL;
    /**
     * The type of records to add.
     * Required: Yes
     * Type: String
     *
     * Valid Values: A | AAAA | CNAME | MX | NS | PTR | SOA | SPF | SRV | TXT
     * @var string
     */
    protected $Type;
    /**
     * Weighted resource record sets only: Among resource record sets that have the same combination of DNS name and
     * type, a value that determines what portion of traffic for the current resource record set is routed to the
     * associated location.
     *
     * For more information about weighted resource record sets, see Setting Up Weighted Resource Record Sets in the
     * Route 53 Developer Guide.
     *
     * Required: Conditional. Required if you are creating a weighted resource record set.
     * Type: Integer
     * @var int
     */
    protected $Weight;
}

