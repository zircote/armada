<?php

namespace Armada\Resource\AWS\RDS;

use Armada\Resource\AbstractResource;

class DBSubnetGroup extends AbstractResource
{
    const NAME = 'AWS::RDS::DBSubnetGroup';
    /**
     * The description for the DB Subnet Group.
     * Type: String
     * Required: Yes
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $dbSubnetGroupDescription;
    /**
     * The EC2 Subnet IDs for the DB Subnet Group.
     * Type: List of String
     * Required: Yes
     *
     * Update requires: no interruption
     *
     * @var array(string)
     */
    protected $subnetIds = array();
}

