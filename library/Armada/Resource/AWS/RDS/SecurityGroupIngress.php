<?php

namespace Armada\Resource\AWS\RDS;

use Armada\Resource\AbstractResource;

class SecurityGroupIngress extends AbstractResource
{
    const NAME = 'AWS::RDS::SecurityGroupIngress';
    /**
     * The IP range to authorize.
     *
     * For an overview of CIDR ranges, go to the Wikipedia Tutorial.
     * Type: String
     * Required: No
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $cidrip;
    /**
     * The name (ARN) of the AWS::RDS::DBSecurityGroup to which this ingress will be added.
     * Type: String
     * Required: Yes
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $dbSecurityGroupName;
    /**
     * The ID of the VPC or EC2 security group to authorize.
     *
     * For VPC DB security groups, use EC2SecurityGroupId. For EC2 security groups, use EC2SecurityGroupOwnerId and
     * either EC2SecurityGroupName or EC2SecurityGroupId.
     *
     * Type: String
     * Required: No
     *
     * Update requires: replacement

     * @var
     */
    protected $ec2SecurityGroupId;
    /**
     * The name of the EC2 security group to authorize.
     * For VPC DB security groups, use EC2SecurityGroupId. For EC2 security groups, use EC2SecurityGroupOwnerId and
     * either EC2SecurityGroupName or EC2SecurityGroupId.
     * Type: String
     * Required: No
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $ec2SecurityGroupName;
    /**
     * The AWS Account Number of the owner of the EC2 security group specified in the EC2SecurityGroupName parameter.
     * The AWS Access Key ID is not an acceptable value.
     *
     * For VPC DB security groups, use EC2SecurityGroupId. For EC2 security groups, use EC2SecurityGroupOwnerId and
     * either EC2SecurityGroupName or EC2SecurityGroupId.
     *
     * Type: String
     * Required: No
     *
     * Update requires: replacement
     * @var
     */
    protected $ec2SecurityGroupOwnerId;
}

