<?php

namespace Armada\Resource\AWS\RDS;

use Armada\Resource\AbstractResource;

class DBInstance extends AbstractResource
{
    const NAME = 'AWS::RDS::DBInstance';
    /**
     * The allocated storage size specified in gigabytes (GB).
     * If any value is used in the Iops parameter, AllocatedStorage must be at least 100 GB, which corresponds to the
     * minimum Iops value of 1000. If Iops is increased (in 1000 IOPS increments), then AllocatedStorage must also be
     * increased (in 100 GB increments) correspondingly.
     *
     * Required: Yes
     * Type: String
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $allocatedStorage;
    /**
     * The name of the Availability Zone where the DB instance is. You cannot set the AvailabilityZone parameter if the
     * MultiAZ parameter is set to true.
     *
     * Required: No
     * Type: String
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $availabilityZone;
    /**
     * The number of days for which automatic DB snapshots are retained.
     * Required: No
     * Type: String
     *
     * Update requires: no interruption
     * @var string
     */
    protected $backupRetentionPeriod;
    /**
     * The name of the compute and memory capacity class of the DB instance.
     * Required: Yes
     * Type: String
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $dbInstanceClass;
    /**
     * The name of the initial database of this instance that was provided at create time, if one was specified when
     * the DB instance was created. This same name is returned for the life of the DB instance.
     *
     * Required: No
     * Type: String
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $dbName;
    /**
     * The name of an existing DB parameter group or a reference to an AWS::RDS::DBParameterGroup resource created in
     * the template.
     * Required: No
     * Type: String
     *
     * Update requires: some interruptions However, if any of the data members of the referenced parameter group are
     * changed during an update, the database instance may need to be restarted, causing some interruption.
     *
     * @var string
     */
    protected $dbParameterGroupName;
    /**
     * A list containing the DB security groups to assign to the Amazon RDS instance. The list can contain both the
     * name of existing DB security groups or references to AWS::RDS::DBSecurityGroup resources created in the template.
     * Required: No
     * Type: List of Strings
     *
     * Update requires: no interruption
     *
     * @var array(string)
     */
    protected $dbSecurityGroups = array();
    /**
     * The identifier for the DB snapshot to restore from.
     * By specifying this property, you can create a DB instance from the specified DB snapshot. If the
     * DBSnapshotIdentifier property is an empty string or the AWS::RDS::DBInstance declaration has no
     * DBSnapshotIdentifier property, the database is created as a new database. If the property contains a value
     * (other than empty string), AWS CloudFormation creates a database from the specified snapshot. If no snapshot
     * exists with the specified name, the database creation fails and the stack rolls back.
     *
     * Required: No
     * Type: String
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $dbSnapshotIdentifier;
    /**
     * A DB Subnet Group to associate with this DB instance.
     * If there is no DB Subnet Group, then it is a non-VPC DB instance.
     * For more information about using Amazon RDS in a VPC, go to Using Amazon RDS with Amazon Virtual Private Cloud
     * (VPC) in the Amazon Relational Database Service Developer Guide.
     *
     * Required: No
     * Type: String
     *
     * Update requires: replacement
     *
     * @var
     */
    protected $dbSubnetGroupName;
    /**
     * The name of the database engine to be used for this DB instance.
     * Required: Yes
     * Type: String
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $engine;
    /**
     * The version number of the database engine to use.
     * Required: No
     * Type: String
     * Update requires: no interruption
     *
     * @var string
     */
    protected $engineVersion;
    /**
     * The number of I/O operations per second (IOPS) that the database should provision. This can be any value from
     * 1000–10,000, in 1000 IOPS increments.
     *
     * If any value is used in the Iops parameter, AllocatedStorage must be at least 100 GB, which corresponds to the
     * minimum Iops value of 1000. If Iops is increased (in 1000 IOPS increments), then AllocatedStorage must also be
     * increased (in 100 GB increments) correspondingly.
     *
     * For more information about this parameter, see Provisioned IOPS in the Amazon Relational Database Service User
     * Guide.
     *
     * Required: No
     * Type: Integer
     *
     * Update requires: some interruptions
     *
     * @var int
     */
    protected $iops;
    /**
     * The license model information for this DB instance.
     * Required: No
     * Type: String
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $licenseModel;
    /**
     * The master username for the DB instance.
     * Required: Yes
     * Type: String
     * Update requires: replacement
     *
     * @var string
     */
    protected $masterUsername;
    /**
     * The master password for the DB instance.
     * Required: Yes
     * Type: String
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $masterUserPassword;
    /**
     * Specifies if the DB instance is a multiple availability-zone deployment. You cannot set the AvailabilityZone
     * parameter if the MultiAZ parameter is set to true.
     *
     * Required: No
     * Type: Boolean
     * Update requires: no interruption
     *
     * @var
     */
    protected $multiAZ;
    /**
     * The port for the instance.
     * Required: No
     * Type: String
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $port;
    /**
     * The daily time range during which automated backups are created if automated backups are enabled, as determined
     * by the BackupRetentionPeriod.
     *
     * Required: No
     * Type: String
     * Update requires: no interruption
     *
     * @var
     */
    protected $preferredBackupWindow;
    /**
     * The weekly time range (in UTC) during which system maintenance can occur.
     * Required: No
     * Type: String
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $preferredMaintenanceWindow;

}

