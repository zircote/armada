<?php
namespace Armada\Resource\AWS\RDS;

/**
 * @package
 * @category
 * @subcategory
 */

use Armada\Resource\AbstractResource;

/**
 * @package
 * @category
 * @subcategory
 */
class DBParameterGroup extends AbstractResource
{
    const NAME = 'AWS::RDS::DBParameterGroup';
    /**
     * A friendly description of the RDS parameter group. For example, "My Parameter Group".
     * Type: String
     *
     * @var string
     */
    protected $description;
    /**
     * The database family of this RDS parameter group. For example, "MySQL5.1".
     * Type: String
     *
     * @var string
     */
    protected $family;
    /**
     * The parameters to set for this RDS parameter group.
     *
     * Type: DBParameters, a JSON object consisting of key/value pairs of Strings. For example:
     *
     * <code>
     * "Parameters" : {
     *    "Key1" : "Value1",
     *    "Key2" : "Value2",
     *    "Key3" : "Value3"
     * }
     * </code>
     *
     * @var array(KeyValue)
     */
    protected $parameters = array();
}
