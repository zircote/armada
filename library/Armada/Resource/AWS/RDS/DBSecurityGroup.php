<?php

namespace Armada\Resource\AWS\RDS;

use Armada\Resource\AbstractResource;

class DBSecurityGroup extends AbstractResource
{
    const NAME = 'AWS::RDS::DBSecurityGroup';
    /**
     * The Id of VPC. Indicates which VPC this DB Security Group should belong to.
     * Type: String
     * Required: Conditional. Must be specified to create a DB Security Group for a VPC; may not be specified otherwise.
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $ec2VpcId;
    /**
     * Network ingress authorization for an Amazon EC2 security group or an IP address range.
     * Type: List of RDS Security Group Rules.
     * Required: Yes
     *
     * Update requires: no interruption
     *
     * @var array(RDS\SecurityGroupRule)
     */
    protected $dbSecurityGroupIngress;
    /**
     * Description of the security group.
     * Type: String
     * Required: Yes
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $groupDescription;
}

