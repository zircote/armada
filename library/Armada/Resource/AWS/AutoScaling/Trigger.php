<?php

namespace Armada\Resource\AWS\AutoScaling;

use Armada\Resource\AbstractResource;

class Trigger extends AbstractResource
{
    const NAME = 'AWS::AutoScaling::Trigger';
    /**
     * The name of the Auto Scaling group.
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $autoScalingGroupName;
    /**
     * The amount of time to wait while the trigger is firing before performing scaling activities in response to the
     * breach.
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $breachDuration;
    /**
     * A list of CloudWatch dimensions used to retrieve metric statistics that the trigger uses to determine when to
     * fire.
     *
     * Required: Yes
     * Type: List of CloudWatch dimensions
     *
     * @var \MetricDimension
     */
    protected $dimensions;
    /**
     * The incremental amount to use when performing scaling activities when the lower threshold has been breached.
     * Must be a negative or positive integer, or integer percentage value.
     *
     * Required: Conditional
     * Type: String
     *
     * @var string
     */
    protected $lowerBreachScaleIncrement;
    /**
     * The lower limit for the metric. The trigger fires if all data points in the last BreachDuration seconds exceed
     * the upper threshold or fall below the lower threshold.
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $lowerThreshold;
    /**
     * The metric name used by the trigger to determine when to fire.
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $metricName;
    /**
     * The namespace used by the trigger to determine when to fire.
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $namespace;
    /**
     * The period used in retrieving metric statistics used by the trigger to determine when to fire.
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $period;
    /**
     * The statistic used by the trigger to determine which metric statistics to examine.
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $statistic;
    /**
     * The standard unit associated with a measure, used by the trigger when fetching the metric statistics it uses to
     * determine when to activate.
     *
     * Required: No
     * Type: String
     *
     * @var string
     */
    protected $unit;
    /**
     * The incremental amount to use when performing scaling activities when the upper threshold has been breached.
     *
     * Must be a negative or positive integer, or integer percentage value.
     *
     * Required: Conditional
     * Type: String
     *
     * @var string
     */
    protected $upperBreachScaleIncrement;
    /**
     * The upper limit of the metric used. The trigger fires if all data points in the last BreachDuration seconds
     * exceed the upper threshold or fall below the lower threshold.
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $upperThreshold;
}

