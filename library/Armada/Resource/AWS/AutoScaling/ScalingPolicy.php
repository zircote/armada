<?php

namespace Armada\Resource\AWS\AutoScaling;

use Armada\Resource\AbstractResource;

class ScalingPolicy extends AbstractResource
{
    const NAME = 'AWS::AutoScaling::ScalingPolicy';
    /**
     * Specifies whether the ScalingAdjustment is an absolute number or a percentage of the current capacity. Valid
     * values are:
     *      - ChangeInCapacity
     *      - ExactCapacity
     *      - PercentChangeInCapacity.
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $adjustmentType;
    /**
     * The name or Amazon Resource Name (ARN) of the Auto Scaling Group that you want to attach the policy to.
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $autoScalingGroupName;
    /**
     * The amount of time, in seconds, after a scaling activity completes before any further trigger-related scaling
     * activities can start.
     *
     * Required: No
     * Type: String
     *
     * @var string
     */
    protected $cooldown;
    /**
     * The number of instances by which to scale. AdjustmentType determines the interpretation of this number (e.g., as
     * an absolute number or as a percentage of the existing Auto Scaling group size). A positive increment adds to the
     * current capacity and a negative value removes from the current capacity.
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $scalingAdjustment;

}

