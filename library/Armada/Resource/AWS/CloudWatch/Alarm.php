<?php

namespace Armada\Resource\AWS\CloudWatch;

/**
 *
 * The AWS::CloudWatch::Alarm type creates an Amazon CloudWatch alarm.
 * This type supports updates. For more information about updating this resource, see PutMetricAlarm. For more
 * information about updating stacks, see Updating AWS CloudFormation Stacks.
 * When you specify an AWS::CloudWatch::Alarm type as an argument to the Ref function, AWS CloudFormation returns the
 * value of the AlarmName.
 *
 */
class Alarm
{
    const NAME = 'AWS::CloudWatch::Alarm';
    const GREATER_THAN_OR_EQUAL = 'GreaterThanOrEqualToThreshold';
    const GREATER_THAN = 'GreaterThanThreshold';
    const LESS_THAN = 'LessThanThreshold';
    const LESS_THAN_OR_EQUAL = 'LessThanOrEqualToThreshold';
    /**
     * Indicates whether or not actions should be executed during any changes to the alarm's state. Either true or
     * false.
     * Required: No
     * Type: String
     * @var string
     */
    protected $actionsEnabled;
    /**
     * The list of actions to execute when this alarm transitions into an ALARM state from any other state. Each action
     * is specified as an Amazon Resource Number (ARN). Currently the only action supported is publishing to an Amazon
     * SNS topic or an Amazon Auto Scaling policy.
     * Required: No
     * Type: List of String
     *
     * @var array
     */
    protected $alarmActions;
    /**
     * The description for the alarm.
     * Required: No
     * Type: String
     *
     * @var string
     */
    protected $alarmDescription;
    /**
     * The arithmetic operation to use when comparing the specified Statistic and Threshold. The specified Statistic
     * value is used as the first operand.
     * Valid Values: GreaterThanOrEqualToThreshold | GreaterThanThreshold | LessThanThreshold | LessThanOrEqualToThreshold
     * Required: Yes
     * Type: String
     *
     * @var \Armada\Resource\CloudWatch\Operator\AbstractOperator;
     */
    protected $comparisonOperator;
    /**
     * The dimensions for the alarm's associated metric.
     * Required: No
     * Type: List of Metric Dimension type
     * @var array(\MetricDimension)
     */
    protected $dimensions = array();
    /**
     * The number of periods over which data is compared to the specified threshold.
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $evaluationPeriods;
    /**
     * The list of actions to execute when this alarm transitions into an INSUFFICIENT_DATA state from any other state.
     * Each action is specified as an Amazon Resource Number (ARN). Currently the only action supported is publishing
     * to an Amazon SNS topic or an Amazon Auto Scaling policy.
     * Required: No
     * Type: List of String
     * @var array(string)
     */
    protected $insufficientDataActions = array();
    /**
     * The name for the alarm's associated metric.
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $metricName;
    /**
     * The namespace for the alarm's associated metric.
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $namespace;
    /**
     * The list of actions to execute when this alarm transitions into an OK state from any other state. Each action is
     * specified as an Amazon Resource Number (ARN). Currently the only action supported is publishing to an Amazon SNS
     * topic or an Amazon Auto Scaling policy.
     * Required: Yes
     * Type List of String
     *
     * @var array(string)
     */
    protected $oKActions = array();
    /**
     * The period in seconds over which the specified statistic is applied.
     * Required: Yes
     * Type: string
     *
     * @var string
     */
    protected $period;
    /**
     * The statistic to apply to the alarm's associated metric.
     *
     * Valid Values: SampleCount | Average | Sum | Minimum | Maximum
     * @var
     */
    protected $statistic = array();
    /**
     * The value against which the specified statistic is compared.
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $threshold;
    /**
     * The unit for the alarm's associated metric.
     * Valid Values: Seconds | Microseconds | Milliseconds | Bytes | Kilobytes | Megabytes | Gigabytes | Terabytes |
     * Bits | Kilobits | Megabits | Gigabits | Terabits | Percent | Count | Bytes/Second | Kilobytes/Second |
     * Megabytes/Second | Gigabytes/Second | Terabytes/Second | Bits/Second | Kilobits/Second | Megabits/Second |
     * Gigabits/Second | Terabits/Second | Count/Second | None
     *
     * @var string
     */
    protected $unit;
}

