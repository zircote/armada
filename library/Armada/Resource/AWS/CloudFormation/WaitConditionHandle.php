<?php
namespace Armada\Resource\CloudFormation;

/**
 * @package     Armada
 * @category    Resource
 * @subcategory CloudFormation
 */
use Armada\Resource\AbstractResource;

class WaitConditionHandle extends AbstractResource
{

    const NAME = 'AWS::CloudFormation::WaitConditionHandle';
}

