<?php

namespace Armada\Resource\AWS\CloudFormation;

use Armada\Resource\AbstractResource;

class WaitCondition extends AbstractResource
{
    const NAME = 'AWS::CloudFormation::WaitCondition';
    /**
     * A reference to the wait condition handle used to signal this wait condition. Use the Ref intrinsic function to
     * specify an AWS::CloudFormation::WaitConditionHandle resource.
     *
     * Required: Yes
     * Type: Reference
     *
     * @var WaitConditionHandle
     */
    protected $handle;
    /**
     * The length of time (in seconds) to wait for the number of signals that the Count property specifies.
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $timeout;
    /**
     * The number of success signals that AWS CloudFormation must receive before it continues the stack creation
     * process. When the wait condition receives requisite the number of success signals, AWS CloudFormation resumes
     * creating the stack. If the wait condition does not receive the specified number of success signals before the
     * Timeout period expires, AWS CloudFormation assumes the wait condition has failed and rolls the stack back.
     *
     * Required: No
     * Type: String
     *
     * @var string
     */
    protected $count;

}

