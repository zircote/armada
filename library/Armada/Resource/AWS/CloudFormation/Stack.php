<?php

namespace Armada\Resource\AWS\CloudFormation;

use Armada\Resource\AbstractResource;

class Stack extends AbstractResource
{
    const NAME = 'AWS::CloudFormation::Stack';
    /**
     * The URL of a template that specifies the stack that you want to create as a resource.
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $templateUrl;
    /**
     * The length of time, in minutes, that AWS CloudFormation waits for the embedded stack to reach the CREATE_COMPLETE
     * state. The default is no timeout. When AWS CloudFormation detects that the embedded stack has reached the
     * CREATE_COMPLETE state, it marks the embedded stack resource as CREATE_COMPLETE in the parent stack and resumes
     * creating the parent stack. If the timeout period expires before the embedded stack reaches CREATE_COMPLETE,
     * AWS CloudFormation marks the embedded stack as failed and rolls back both the embedded stack and parent stack.
     *
     * Required: No
     * Type: String
     *
     * @var string
     */
    protected $timeoutInMinutes;
    /**
     * The set of parameters passed to AWS CloudFormation when this embedded stack is created.
     *
     * Required: Conditional
     * Type: CloudFormation Stack Parameters
     *
     * @var \AbstractParameters
     */
    protected $parameters;

}

