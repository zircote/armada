<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 *
 * You can use the sources key to download an archive file and unpack it in a target directory on the EC2 instance.
 *
 * <b>Supported formats:</b>
 * Supported formats are tar, tar+gzip, tar+bz2 and zip.
 *
 * <b>GitHub</b>
 * If you use GitHub as a source control system, you can use cfn-init and the sources package mechanism to pull a
 * specific version of your application. GitHub allows you to create a zip or a tar from a specific version via a URL
 * as follows:
 *
 * <code>
 * https://github.com/<your directory>/(zipball|tarball)/<version>
 * </code>
 *
 * For example, the following snippet pulls down version master as a tarfile.
 *
 * </code>
 * "sources" : {
 *   "/etc/puppet" : https://github.com/user1/cfn-demo/tarball/master
 *   }
 * </code>
 *
 * <b>Example</b>:
 * The following example downloads a zip file from an Amazon S3 bucket and unpacking it into /etc/myapp:
 * <code>
 * "sources" : {
 *   "/etc/myapp" : "https://s3.amazonaws.com/mybucket/myapp.tar.gz"
 *   }
 * </code>
 * You can include authentication credentials for a source by using the AWS::CloudFormation::Authentication resource.
 */
class Sources
{
    /**
     * @var array(target_directory => package)
     */
    protected $sources = array();
}
