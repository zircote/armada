<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 *
 * You can use the files key to create files on the EC2 instance. The content can be either inline in the template or
 * the content can be pulled from a URL. The files are written to disk in lexicographic order. The following table
 * lists the supported keys.
 *
 * <code>
 * "files" : {
 *   "/tmp/setup.mysql" : {
 *     "content" : { "Fn::Join" : ["", [
 *       "CREATE DATABASE ", { "Ref" : "DBName" }, ";\n",
 *       "CREATE USER '", { "Ref" : "DBUsername" }, "'@'localhost' IDENTIFIED BY '",
 *                        { "Ref" : "DBPassword" }, "';\n",
 *       "GRANT ALL ON ", { "Ref" : "DBName" }, ".* TO '", { "Ref" : "DBUsername" },
 *                        "'@'localhost';\n",
 *       "FLUSH PRIVILEGES;\n"
 *       ]]},
 *     "mode"  : "000644",
 *     "owner" : "root",
 *     "group" : "root"
 *   }
 * },
 * </code>
 */
class Files
{
    /**
     * Either a string or a properly formatted JSON object. If you use a JSON object as your content, the JSON will be
     * written to a file on disk. Any intrinsic functions such as Fn::GetAtt or Ref are evaluated before the JSON object
     * is written to disk.
     * @var string
     */
    protected $content;
    /**
     * A URL to load the file from. This option cannot be specified with the content key.
     * @var
     */
    protected $source;
    /**
     * The encoding format. Only used if the content is a string. Encoding is not applied if you are using a source.
     * Valid values: plain | base64
     *
     * @var
     */
    protected $encoding;
    /**
     * The name of the owning group for this file.
     * @var
     */
    protected $group;
    /**
     * The name of the owning user for this file.
     * @var
     */
    protected $owner;
    /**
     * A six-digit octal value representing the mode for this file.
     * @var
     */
    protected $mode;
    /**
     * The name of an authentication method to use. This overrides any default authentication. You can use this
     * property to select an authentication method you define with the AWS::CloudFormation::Authentication resource.
     * @var
     */
    protected $authentication;
}
