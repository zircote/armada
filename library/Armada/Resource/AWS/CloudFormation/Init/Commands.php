<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 * You can use the commands key to execute commands on the EC2 instance. The commands are processed in alphabetical
 * order by name.
 *
 * <code>
 * "commands" : {
 *     "test" : {
 *         "command" : "echo \"$MAGIC\" > test.txt",
 *         "env" : { "MAGIC" : "I come from the environment!" },
 *         "cwd" : "~",
 *         "test" : "test ! -e ~/test.txt",
 *         "ignoreErrors" : "false"
 *     }
 * }
 * </code>
 */
class Command
{
    /**
     * Required. Either an array or a string specifying the command to run. If you use an array, you do not need to
     * escape space characters or enclose command parameters in quotes.
     *
     * @var array
     */
    protected $command = array();
    /**
     * Optional. Sets environment variables for the command. This property overwrites, rather than appends, the
     * existing environment.
     * @var
     */
    protected $env;
    /**
     * Optional. The working directory
     * @var
     */
    protected $cwd;
    /**
     * Optional. A command that must return the value true in order for cfn-init to process the command contained in
     * the command key.
     * @var
     */
    protected $test;
    /**
     * Optional. A boolean value that determines whether cfn-init continues to run if the command in contained in the
     * command key fails (returns a non-zero value). Set to true if you want cfn-init to continue running even if the
     * command fails. Set to false if you want cfn-init to stop running if the command fails. The default value is
     * false.
     *
     * @var
     */
    protected $ignoreErrors;

}
