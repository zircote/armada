<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 *
 * <code>
 * "rpm" : {
 *   "epel" : "http://download.fedoraproject.org/pub/epel/5/i386/epel-release-5-4.noarch.rpm"
 * },
 * "yum" : {
 *   "httpd" : [],
 *   "php" : [],
 *   "wordpress" : []
 * },
 * "rubygems" : {
 *   "chef" : [ "0.10.2" ]
 * }
 * </code>
 */
class Packages
{
    /**
     * @var array(package_name => (version|null))
     */
    protected $apt = array();
    /**
     * @var array(package_name => (version|null))
     */
    protected $yum = array();
    /**
     * @var array(package_name => (version|null))
     */
    protected $rubygems = array();
    /**
     * @var array(package_name => (version|null))
     */
    protected $python = array();
    /**
     * @var array(package_name => (version|null))
     */
    protected $rpm = array();
}
