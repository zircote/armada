<?php
/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 *
 * <code>
 * "groups" : {
 *     "groupOne" : {},
 *     "groupTwo" : { "gid" : "45" }
 * }
 * </code>
 */
class Groups
{
    /**
     * @var array(group_name => (gid|null))
     */
    protected $groups = array();
}
