<?php
namespace Armada\Resource\CloudFormation;

/**
 * @package     Armada
 * @category    Resource
 * @subcategory CloudFormation
 */
use Armada\Resource\AbstractResource;

/**
 * Use the AWS::CloudFormation::Authentication type to specify authentication credentials for files or sources that you
 * specify with the AWS::CloudFormation::Init type.
 *
 * To include authentication information for a file or source that you specify with AWS::CloudFormation::Init, use the
 * uris property if the source is a URI or the buckets property if the source is an Amazon S3 bucket. For more
 * information about files, see Files. For more information about sources, see Sources.
 *
 * You can also specify authentication information for files directly in the AWS::CloudFormation::Init resource. The
 * files key of the resource contains a property named authentication. You can use the authentication property to
 * associate authentication information defined in an AWS::CloudFormation::Authentication resource directly with a file.
 *
 * For files, AWS CloudFormation looks for authentication information in the following order:
 *      - The authentication property of the AWS::CloudFormation::Init files key.
 *      - The uris or buckets property of the AWS::CloudFormation::Authentication resource.
 *
 * For sources, AWS CloudFormation looks for authentication information in the uris or buckets property of the
 * AWS::CloudFormation::Authentication resource.
 *
 * @see http://docs.amazonwebservices.com/AWSCloudFormation/latest/UserGuide/aws-resource-authentication.html
 */
class Authentication extends AbstractResource
{
    const NAME = 'AWS::CloudFormation::Authentication';

    /**
     * Required. Specifies whether the authentication scheme uses a username and password (basic) or an access key ID
     * and secret key (S3).
     *
     * If you specify basic, you must also specify the username, password, and uris properties.
     * If you specify S3, you must also specify the accessKeyId, secretKey, and buckets properties.
     *
     * Valid values: basic | S3
     *
     * @var string
     */
    protected $type;
    /**
     * Conditional. Specifies the username for basic authentication.
     * Condition: Can be specified only if the type property is basic.
     *
     * @var string
     */
    protected $username;
    /**
     * Conditional. Specifies the password for basic authentication.
     * Condition: Can be specified only if the type property is basic.
     *
     * @var string
     */
    protected $password;
    /**
     * Conditional. A comma-delimited list of URIs to be associated with the basic authentication credentials. The
     *  authorization applies to the specified URIs and any more specific URI. For example, if you specify
     *  http://www.example.com, the authorization will also apply to http://www.example.com/test.
     * Condition: Can be specified only if the type property is basic.
     *
     * @var string
     */
    protected $uris;
    /**
     * Conditional. Specifies the access key ID for S3 authentication.
     * Condition: Can be specified only if the type property is S3.
     *
     * @var string
     */
    protected $accessKeyId;
    /**
     * Conditional. Specifies the secret key for S3 authentication.
     * Condition: Can be specified only if the type property is S3.
     *
     * @var string
     */
    protected $secretKey;
    /**
     * Conditional. A comma-delimited list of Amazon S3 buckets to be associated with the S3 authentication credentials.
     * Condition: Can be specified only if the type property is S3.
     *
     * @var string
     */
    protected $buckets;
}

