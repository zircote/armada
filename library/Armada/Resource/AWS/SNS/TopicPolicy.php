<?php

namespace Armada\Resource\AWS\SNS;

use Armada\Resource\AbstractResource;

class TopicPolicy extends AbstractResource
{
    const NAME = 'AWS::SNS::TopicPolicy';
    /**
     * A policy document containing permissions to add to the specified SNS topics.
     * Required: Yes
     * Type: JSON
     *
     * @var string
     */
    protected $PolicyDocument;
    /**
     * The Amazon Resource Names (ARN) of the topics to which you want to add the policy. You can use the Ref function
     * to specify an AWS::SNS::Topic resource.
     *
     * Required: Yes
     * Type: Array of SNS Topic ARNs
     *
     * @var array(string)
     */
    protected $topics;

}

