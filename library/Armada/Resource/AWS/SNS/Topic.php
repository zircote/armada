<?php

namespace Armada\Resource\AWS\SNS;

use Armada\Resource\AbstractResource;

class Topic extends AbstractResource
{
    const NAME = 'AWS::SNS::Topic';
    /**
     * A developer-defined string that can be used to identify this SNS topic.
     * Required: No
     * Type: String
     *
     * @var string
     */
    protected $displayName;
    /**
     * The SNS subscriptions (endpoints) for this topic.
     * Required: Yes
     * Type: List of SNS Subscriptions
     * @var array(SNS\Subscriptions)
     */
    protected $subscription = array();
}

