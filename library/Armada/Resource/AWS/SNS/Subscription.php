<?php

namespace Armada\Resource\AWS\SNS;

use Armada\Resource\AbstractResource;

class Subscription extends AbstractResource
{
    const NAME = 'AWS::SNS::Subscription';
    /**
     * The length of time during which the queue will be unavailable once a message is delivered from the queue. This
     * blocks other components from receiving the same message and gives the initial component time to process and
     * delete the message from the queue.
     *
     * Values must be from 0 to 43200 seconds (12 hours). If no value is specified, the default value of 30 seconds will
     * be used.
     *
     * For more information about SQS Queue visibility timeouts, see Visibility Timeout in the Amazon Simple Queue
     * Service Developer Guide.
     *
     * Required: No
     * Type: Integer
     *
     * @var integer
     */
    protected $visibilityTimeout;
}

