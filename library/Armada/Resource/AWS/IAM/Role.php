<?php

namespace Armada\Resource\AWS\IAM;

use Armada\Resource\AbstractResource;

class Role extends AbstractResource
{
    const NAME = 'AWS::IAM::Role';
    /**
     * The IAM assume role policy pocument associated with this role.
     * Required: Yes
     * Type: A JSON policy document.
     *
     * Update requires: no interruption
     *
     * Note: There can be only one assume role policy document associated with a role, and currently the only policy
     * supported allows an EC2 instance to assume the role. For an example of the allowed policy document, see Template
     * Examples.
     *
     * @var Json(object)
     */
    protected $assumeRolePolicyDocument;
    /**
     * The path associated with this role. For information about IAM paths, see Friendly Names and Paths in the AWS
     * Identity and Access Management User Guide.
     * Required: Yes
     * Type: String
     *
     * Update requires: replacement
     * @var
     */
    protected $path;
    /**
     * A list of embedded policies to associate with this role. Policies can also be specified externally. For sample
     * templates that demonstrates both embedded and external policies, see Template Examples.
     * Required: No
     * Type: A list of IAM policies.
     *
     * Update requires: no interruption
     *
     * @var array
     */
    protected $policies = array();

}

