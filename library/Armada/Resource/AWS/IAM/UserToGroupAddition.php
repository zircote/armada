<?php

namespace Armada\Resource\AWS\IAM;

use Armada\Resource\AbstractResource;

class UserToGroupAddition extends AbstractResource
{

    const NAME = 'AWS::IAM::UserToGroupAddition';
    /**
     * The name of group to add users to.
     * Required: Yes
     * Type: String
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $groupName;
    /**
     * Required: Yes
     * Type: List of users
     * Update requires: no interruption
     *
     * @var array(IAM\User)
     */
    protected $users = array();

}

