<?php

namespace Armada\Resource\AWS\IAM;

use Armada\Resource\AbstractResource;

class User extends AbstractResource
{
    const NAME = 'AWS::IAM::User';
    /**
     * The path for the user name. For more information about paths, see Identifiers for IAM Entities in Using AWS
     * Identity and Access Management.
     * Required: No
     * Type: String
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $path;
    /**
     * The names of groups to which you want to add the user.
     * Required: No
     * Type: List of Groups
     *
     * Update requires: no interruption
     *
     * @var array(string)
     */
    protected $groups;
    /**
     * Creates a login profile for the user so the user can access AWS services such as the AWS Management Console.
     *
     * The LoginProfile type is an embedded property in the AWS::IAM::User type. The LoginProfile property contains a single field: Password, which takes a string as its value. For example:
     *
     * <code>
     * "LoginProfile": { "Password": "myP@ssW0rd" }
     * </code>
     *
     * Required: No
     * Type: LoginProfile type
     *
     * Update requires: no interruption
     *
     * @var LoginProfile
     */
    protected $loginProfile;
    /**
     * Applies specified policies to the user. For information about policies, see Overview of Policies in [Using IAM].
     * Required: No
     * Type: List of AWS::IAM::Policy types
     *
     * Update requires: no interruption
     *
     * @var array(IAM\Policy)
     */
    protected $policies;

}

