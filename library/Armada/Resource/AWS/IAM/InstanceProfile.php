<?php

namespace Armada\Resource\AWS\IAM;

use Armada\Resource\AbstractResource;

class InstanceProfile extends AbstractResource
{
    const NAME = 'AWS::IAM::InstanceProfile';
    /**
     * The path associated with this IAM instance profile. For information about IAM paths, see Friendly Names and
     * Paths in the AWS Identity and Access Management User Guide.
     *
     * Required: Yes
     * Type: String
     *
     * Update requires: replacement
     *
     * @var
     */
    protected $path;
    /**
     * The roles associated with this IAM instance profile.
     *
     * Required: Yes
     * Type: List of references to AWS::IAM::Roles. Currently, a maximum of one role can be assigned to an instance
     * profile.
     *
     * Update requires: no interruption
     *
     * @var array(IAM\Roles)
     */
    protected $roles = array();

}

