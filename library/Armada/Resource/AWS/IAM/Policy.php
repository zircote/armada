<?php
namespace Armada\Resource\AWS\IAM;

/**
 *
 */
use Armada\Resource\AbstractResource;

/**
 *
 */
class Policy extends AbstractResource
{
    const NAME = 'AWS::IAM::Policy';
    /**
     * The names (ARNs) of groups to which you want to add the policy.
     * Required: Conditional
     * Type: a list of strings
     *
     * Update requires: no interruption
     *
     * @var array(string)
     */
    protected $groups = array();
    /**
     * A policy document that contains permissions to add to the specified users or groups.
     * Required: Yes
     * Type: JSON object
     *
     * Update requires: no interruption
     *
     * @var JSON(object)
     */
    protected $policyDocument;
    /**
     * The name of the policy.
     * Required: Yes
     * Type: String
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $policyName;
    /**
     * The names (ARNs) of AWS::IAM::Roles to attach to this policy.
     * Required: No
     * Type: a list of strings
     * Update requires: no interruption
     *
     * @var array(string)
     */
    protected $roles = array();
    /**
     * The names of users for whom you want to add the policy.
     * Required: Conditional
     * Type: a list of strings
     *
     * Update requires: no interruption
     *
     * @var array(string)
     */
    protected $users = array();
}

