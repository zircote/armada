<?php

namespace Armada\Resource\AWS\ElasticLoadBalancing;

use Armada\Resource\AbstractResource;

class LoadBalancer extends AbstractResource
{
    const NAME = 'AWS::ElasticLoadBalancing::LoadBalancer';
    /**
     * Generates one or more stickiness policies with sticky session lifetimes that follow that of an
     * application-generated cookie. These policies can be associated only with HTTP/HTTPS listeners.
     *
     * Required: No
     * Type: A list of AppCookieStickinessPolicy objects.
     *
     * @var \AppCookieStickinessPolicy
     */
    protected $appCookieStickinessPolicy;
    /**
     * The Availability Zones in which to create the load balancer. You can specify either AvailabilityZones or Subnets,
     * but not both.
     *
     * Required: No
     * Type: a list of strings
     *
     * @var string
     */
    protected $availabilityZones;
    /**
     * When specified, declares an application health check for the instances.
     *
     * Required: No
     * Type: ElasticLoadBalancing HealthCheck Type.
     *
     * @var \HealthCheck
     */
    protected $healthCheck;
    /**
     * Provides a list of EC2 instance IDs for the load balancer.
     *
     * Required: No
     * Type: a list of strings
     *
     * @var string
     */
    protected $instances;
    /**
     * Generates a stickiness policy with sticky session lifetimes controlled by the lifetime of the browser
     * (user-agent), or by a specified expiration period. This policy can be associated only with HTTP/HTTPS listeners.
     *
     * Required: No
     * Type: A list of LBCookieStickinessPolicy objects.
     *
     * @var \LBCookieStickinessPolicy
     */
    protected $lbCookieStickinessPolicy;
    /**
     * One or more listeners for this load balancer. Each listener must be registered for a specific port, and you
     * cannot have more than one listener for a given port.
     *
     * Required: Yes
     * Type: A list of ElasticLoadBalancing Listener Property Type objects.
     *
     * @var \Listener
     */
    protected $listeners;
    /**
     * A list of elastic load balancing policies to apply to this elastic load balancer.
     *
     * Required: No
     * Type: A list of ElasticLoadBalancing policy objects.
     *
     * @var array(\ElasticLoadBalancingPolicy)
     */
    protected $policies = array();
    /**
     * A list of elastic load balancing policies to apply to this elastic load balancer.
     *
     * Required: No
     * Type: A list of ElasticLoadBalancing policy objects.
     *
     * @var
     */
    protected $scheme;
    /**
     * Required: No
     * Type: A list of security groups assigned to your load balancer within your virtual private cloud (VPC).
     *
     * Update requires: no interruption
     *
     * @var array(\SecurityGroup)
     */
    protected $securityGroups;
    /**
     * A list of subnet IDs in your virtual private cloud (VPC) to attach to your load balancer. You can specify either
     * AvailabilityZones or Subnets, but not both.
     *
     * Required: No
     * Type: a list of strings
     *
     * For more information about using Elastic Load Balancing in a VPC, see How Do I Use Elastic Load Balancing in
     * Amazon VPC in the Elastic Load Balancing Developer Guide.
     *
     * @var string
     */
    protected $subnets;

}

