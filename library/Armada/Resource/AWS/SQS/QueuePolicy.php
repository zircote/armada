<?php

namespace Armada\Resource\AWS\SQS;

use Armada\Resource\AbstractResource;

class QueuePolicy extends AbstractResource
{
    const NAME = 'AWS::SQS::QueuePolicy';
    /**
     * A policy document containing permissions to add to the specified SQS queues.
     * Type: JSON
     * Required: Yes
     *
     * @var JSON(object)
     *
     */
    protected $policyDocument;
    /**
     * The URLs of the queues to which you want to add the policy. You can use the Ref function to specify an
     * AWS::SQS::Queue resource.
     *
     * Type: List of Queue URLs
     * Required: Yes
     *
     * @var array(string)
     */
    protected $queues = array();

}

