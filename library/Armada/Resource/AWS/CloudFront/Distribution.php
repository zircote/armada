<?php

namespace Armada\Resource\AWS\CloudFront;

use Armada\Resource\AbstractResource;

class Distribution extends AbstractResource
{

    const NAME = 'AWS::CloudFront::Distribution';
    /**
     * The distribution's configuration information.
     *
     * Required: Yes
     * Type: DistributionConfig type
     *
     * Update requires: replacement
     *
     * @var \DistributionConfig
     */
    protected $distributionConfig;

}

