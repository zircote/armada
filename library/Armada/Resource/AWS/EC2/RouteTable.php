<?php

namespace Armada\Resource\AWS\EC2;

class RouteTable
{
    /**
     * The ID of the VPC where the route table will be created.
     *
     * Example: vpc-11ad4878
     * Required: Yes
     * Type: String
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $vpcId;
    /**
     * The tags you want to attach to this resource.
     *
     * Required: No
     * Type: List of EC2 Tags
     *
     * Update requires: no interruption
     *
     * @var array(Ec2\Tag)
     */
    protected $tags = array();

}

