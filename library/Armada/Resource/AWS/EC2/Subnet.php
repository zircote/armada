<?php

namespace Armada\Resource\AWS\EC2;

use Armada\Resource\AbstractResource;

class Subnet extends AbstractResource
{
    const NAME = 'AWS::EC2::Subnet';
    /**
     * The availability zone you want the subnet in. Default: AWS selects a zone for you (recommended).
     *
     * Required: No
     * Type: String
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $availabilityZone;
    /**
     * The CIDR block you want the subnet to cover (for example, "10.0.0.0/24").
     *
     * Required: Yes
     * Type: String
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $cidrBlock;
    /**
     * The tags you want to attach to this resource.
     *
     * Required: No
     * Type: List of EC2 Tags.
     *
     * Update requires: no interruption
     *
     * @var array(Ec2\Tag)
     */
    protected $tags;
    /**
     * A Ref structure containing the ID of the VPC where you want to create the subnet. The VPC ID is provided as the
     * value of the "Ref" property, as: { "Ref": "VPCID" }.
     *
     * Required: Yes
     * Type: Ref ID
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $vpcId;

}

