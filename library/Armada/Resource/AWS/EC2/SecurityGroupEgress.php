<?php

namespace Armada\Resource\AWS\EC2;

class SecurityGroupEgress
{
    /**
     * ID of the Amazon VPC security group to modify. This value can be a reference to an AWS::EC2::SecurityGroup
     * resource that has a valid VpcId property or the ID of an existing Amazon VPC security group.
     *
     * Type: String
     * Required: Yes
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $groupId;
    /**
     * IP protocol name or number. For valid values, see the IpProtocol parameter in AuthorizeSecurityGroupIngress
     *
     * Type: String
     * Required: Yes
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $ipProtocol;
    /**
     * CIDR range.
     *
     * Type: String
     * Required: Conditional—cannot be used when specifying a destination security group.
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $cidrIp;
    /**
     * Specifies the GroupId of the destination Amazon VPC security group.
     * Type: String
     * Required: Conditional—cannot be used when specifying a CIDR IP address.
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $destinationSecurityGroupId;
    /**
     * Start of port range for the TCP and UDP protocols, or an ICMP type number. An ICMP type number of -1 indicates a
     * wildcard (i.e., any ICMP type number).
     *
     * Type: String
     * Required: Yes
     *
     * Update requires: no interruption
     *
     * @var number
     */
    protected $fromPort;
    /**
     * End of port range for the TCP and UDP protocols, or an ICMP code. An ICMP code of -1 indicates a wildcard (i.e.,
     * any ICMP code).
     *
     * Type: String
     * Required: Yes
     *
     * Update requires: no interruption
     *
     * @var number
     */
    protected $toPort;

}

