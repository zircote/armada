<?php

namespace Armada\Resource\AWS\EC2;

use Armada\Resource\AbstractResource;

class VPCDHCPOptionsAssociation extends AbstractResource
{
    const NAME = 'AWS::VPNDHCPOptionsAssociation';
    /**
     * The ID of the VPC to associate with this DHCP options set.
     *
     * Required: Yes
     *
     * @var string
     */
    protected $vpcId;
    /**
     * The ID of the DHCP options you want to associate with the VPC, or "default" if you want the VPC to use no DHCP
     * options.
     *
     * Required: Yes
     *
     * @var string
     */
    protected $dhcpOptionsId;

}

