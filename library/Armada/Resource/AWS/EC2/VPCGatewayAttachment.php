<?php

namespace Armada\Resource\AWS\EC2;

use Armada\Resource\AbstractResource;

class VPCGatewayAttachment extends AbstractResource
{
    const NAME = 'AWS::EC2::VPNGatewayAttachment';
    /**
     * The ID of the VPC to associate with this gateway.
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $vpcId;
    /**
     * The ID of the Internet gateway.
     *
     * Required: Conditional. You must specify either InternetGatewayId or VpnGatewayId, but not both.
     * Type: String
     *
     * @var string
     */
    protected $internetGatewayId;
    /**
     * The ID of the virtual private networ (VPN) gateway to attach to the VPC.
     *
     * Required: Conditional. You must specify either InternetGatewayId or VpnGatewayId, but not both.
     * Type: String
     *
     * @var string
     */
    protected $vpnGatewayId;

}

