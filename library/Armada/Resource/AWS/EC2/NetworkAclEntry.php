<?php

namespace Armada\Resource\AWS\EC2;

class NetworkAclEntry
{
    /**
     * ID of the ACL where the entry will be created.
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $networkAclId;
    /**
     * Rule number to assign to the entry (e.g., 100). This must be a postive integer from 1 to 32766.
     *
     * Required: Yes
     * Type: Integer
     *
     * @var int
     */
    protected $ruleNumber;
    /**
     * IP protocol the rule applies to. You can use -1 to mean all protocols. This must be -1 or a protocol number (go
     * to Protocol Numbers at iana.org).
     *
     * Required: Yes
     * Type: Integer
     *
     * @var int
     */
    protected $protocol;
    /**
     * Whether to allow or deny traffic that matches the rule; valid values are "allow" or "deny".
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $ruleAction;
    /**
     * Whether this rule applies to egress traffic from the subnet ("true") or ingress traffic to the subnet ("false").
     *
     * Required: Yes
     * Type: Boolean
     *
     * @var bool
     */
    protected $egress;
    /**
     * The CIDR range to allow or deny, in CIDR notation (e.g., 172.16.0.0/24).
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $cidrBlock;
    /**
     * The Internet Control Message Protocol (ICMP) code and type.
     * Required: Conditional, required if specifying 1 (ICMP) for the protocol parameter.
     *
     * Type: EC2 ICMP Property Type
     *
     * @var
     */
    protected $icmp;
    /**
     * The range of port numbers for the UDP/TCP protocol.
     * Required: Conditional, required if specifying 6 (TCP) or 17 (UDP) for the protocol parameter.
     *
     * Type: EC2 PortRange Property Type
     *
     * @var
     */
    protected $portRange;
}

