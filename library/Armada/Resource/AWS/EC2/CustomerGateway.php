<?php

namespace Armada\Resource\AWS\EC2;

/**
 * Provides information to AWS about your VPN customer gateway device.
 *
 * <code>
 * {
 *    "AWSTemplateFormatVersion" : "2010-09-09",
 *    "Resources" : {
 *       "myCustomerGateway" : {
 *          "Type" : "AWS::EC2::CustomerGateway",
 *          "Properties" : {
 *             "Type" : "ipsec.1",
 *             "BgpAsn" : "64000",
 *             "IpAddress" : "1.1.1.1"
 *          }
 *       }
 *    }
 * }
 * </code>
 */
class CustomerGateway
{
    /**
     * The type of VPN connection this customer gateway supports.
     * example: ipsec.1
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $type;
    /**
     * The Internet-routable IP address for the customer gateway's outside interface. The address must be static.
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $ipAddress;
    /**
     * The customer gateway's Border Gateway Protocol (BGP) Autonomous System Number (ASN).
     *
     * Required: Yes
     * Type: Integer
     *
     * @var int
     */
    protected $bgpAsn;
}

