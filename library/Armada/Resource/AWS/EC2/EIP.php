<?php

namespace Armada\Resource\AWS\EC2;

class EIP
{

    /**
     * The Instance ID of the Amazon EC2 instance that you want to associate with this Elastic IP address.
     *
     * Required: No
     * Type: String
     *
     * @var string
     */
    protected $instanceId;
    /**
     * Set to "vpc" to allocate the address to your Virtual Private Cloud (VPC). No other values are supported.
     * For more information, see AllocateAddress in the Amazon Elastic Compute Cloud API Reference. For more information
     * about Elastic IP Addresses in VPC, go to Elastic IP Addresses in the Amazon VPC User Guide.
     *
     * Required: Conditional. Required when allocating an address to a VPC
     * Type: String
     *
     * @var string
     */
    protected $domain;

}

