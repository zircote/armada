<?php

namespace Armada\Resource\AWS\EC2;

class NetworkAcl
{

    /**
     * The ID of the VPC where the network ACL will be created.
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $vpcId;
    /**
     * The tags you want to attach to this resource.
     * For more information about tags, go to Using Tags in the Amazon Elastic Compute Cloud User Guide.
     *
     * Required: No
     * Type: List of EC2 Tags
     *
     * Update requires: no interruption
     *
     * @var array
     */
    protected $tags = array();
}

