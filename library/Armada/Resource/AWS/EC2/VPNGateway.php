<?php

namespace Armada\Resource\AWS\EC2;

use Armada\Resource\AbstractResource;

class VPNGateway extends AbstractResource
{
    const NAME = 'AWS::EC2::VPNGateway';
    /**
     * The type of VPN connection this virtual private gateway supports. The only valid value is "ipsec.1".
     *
     * Required: Yes
     * Type: String
     * Update Requires: N/A
     *
     * @var string
     */
    protected $type = 'ipsec.1';
    /**
     * The tags you want to attach to the instance.
     *
     * Required: No
     * Type: List of EC2 Tags
     *
     * Update requires: no interruption
     *
     * @var array(Ec2\Tag
     */
    protected $tags = array();

}

