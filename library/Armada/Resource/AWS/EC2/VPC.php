<?php

namespace Armada\Resource\AWS\EC2;

use Armada\Resource\AbstractResource;

class VPC extends AbstractResource
{
    const NAME = 'AWS::EC2::VPC';
    /**
     * The CIDR block you want the VPC to cover. For example: "10.0.0.0/16".
     *
     * Type: String
     * Required: Yes
     *
     * @var string
     */
    protected $cidrBlock;
    /**
     * The allowed tenancy of instances launched into the VPC. A value of "default" means instances can be launched with
     * any tenancy; a value of "dedicated" means instances must be launched with tenancy as dedicated.
     *
     * Type: String
     * Required: No
     *
     * Valid values: "default" or "dedicated"
     *
     * @var string
     */
    protected $instanceTenancy;
    /**
     * The tags you want to attach to the instance.
     *
     * Type: List of EC2 Tags.
     * Required: No
     *
     * Update requires: no interruption
     *
     * @var array(Ec2\Tag)
     */
    protected $tags = array();

}

