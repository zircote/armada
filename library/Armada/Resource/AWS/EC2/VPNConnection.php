<?php

namespace Armada\Resource\AWS\EC2;

use Armada\Resource\AbstractResource;

class VPNConnection extends AbstractResource
{
    const NAME = 'AWS::EC2::VPNConnection';
    /**
     * The type of VPN connection this virtual private gateway supports.
     *
     * Example: "ipsec.1"
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $type;
    /**
     * The ID of the customer gateway. This can either be an embedded JSON object or a reference to a Gateway ID.
     *
     * Required: Yes
     * Type: String
     *
     * @var GatewayId
     */
    protected $customerGatewayId;
    /**
     * The ID of the virtual private gateway. This can either be an embedded JSON object or a reference to a Gateway ID.
     *
     * Required: Yes
     * Type: String
     *
     * @var GatewayId
     */
    protected $vpnGatewayId;

}

