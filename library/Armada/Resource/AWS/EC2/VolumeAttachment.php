<?php

namespace Armada\Resource\AWS\EC2;

use Armada\Resource\AbstractResource;

class VolumeAttachment extends AbstractResource
{
    const NAME = 'AWS::EC2::VolumeAttachment';
    /**
     * How the device is exposed to the instance (e.g., /dev/sdh, or xvdh)
     * @var string
     */
    protected $device;
    /**
     * The ID of the instance to which the volume attaches. This value can be a reference to an AWS::EC2::Instance
     * resource, or it can be the instance ID of an existing EC2 instance.
     *
     * @var string
     */
    protected $instanceId;
    /**
     * The ID of the Amazon EBS volume. The volume and instance must be within the same Availability Zone. This value
     * can be a reference to an AWS::EC2::Volume (p. 253) resource, or it can be the volume ID of an existing Amazon
     * EBS volume.
     * @var string
     */
    protected $volumeId;

}

