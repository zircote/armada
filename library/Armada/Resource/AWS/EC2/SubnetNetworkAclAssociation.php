<?php

namespace Armada\Resource\AWS\EC2;

use Armada\Resource\AbstractResource;

class SubnetNetworkAclAssociation extends AbstractResource
{
    const NAME = 'AWS::EC2::SubnetNetworkAclAssociation';
    /**
     * The ID representing the current association between the original network ACL and the subnet.
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $subnetId;
    /**
     * The ID of the new ACL to associate with the subnet.
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $networkAclId;

}

