<?php

namespace Armada\Resource\AWS\EC2;

use Armada\Resource\AbstractResource;

class SubnetRouteTableAssociation extends AbstractResource
{
    const NAME = 'AWS::EC2::SubnetRouteTableAssociation';
    /**
     * The ID of the route table. This is commonly written as a reference to a route table declared elsewhere in the
     * template.
     *
     * Required: Yes
     * Type: String
     *
     * Update requires: no interruption. However, the physical ID changes when the route table ID is changed.
     *
     * @var Ref|string
     */
    protected $routeTableId;
    /**
     * The ID of the subnet. This is commonly written as a reference to a subnet declared elsewhere in the template.
     *
     * Required: Yes
     * Type: String
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $subnetId;
}

