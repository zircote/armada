<?php

namespace Armada\Resource\AWS\EC2;

use Armada\Resource\AbstractResource;

class Volume extends AbstractResource
{
    const NAME = 'AWS::EC2::Volume';
    /**
     * The Availability Zone in which to create the new volume.
     *
     * Required: Yes
     * Type: String
     *
     * @var
     */
    protected $availabilityZone;
    /**
     * The number of I/O operations per second (IOPS) that the volume supports. This can be any value from 1–1000.
     *
     * Required: Conditional. Required when the volume type is "io1"; not used with standard volumes.
     * Type: Integer
     *
     * @var int
     */
    protected $iops;
    /**
     * The size of the volume, in gibibytes (GiBs). This can be any value from 1–1024.
     *
     * Required: Conditional. Required if you are not creating a volume from a snapshot. If you specify Size, do not
     * specify SnapshotId.
     * Type: String
     *
     * @var string
     */
    protected $size;
    /**
     * The snapshot from which to create the new volume.
     *
     * Required: Conditional. Required if you are creating a volume from a snapshot. If you do not specify a value for
     * SnapshotId, you should specify a value for Size.
     * Type: String
     *
     * @var string
     */
    protected $snapshotId;
    /**
     * The tags you want to attach to the volume.
     *
     * Required: No
     * Type: List of EC2 Tags
     *
     * @var array(string)
     */
    protected $tags = array();
    /**
     * The volume type. This can be either "standard" or "io1". If no value is specified, "standard" will be used.
     *
     * Required: No
     * Type: String
     *
     * @var string
     */
    protected $volumeType;

}

