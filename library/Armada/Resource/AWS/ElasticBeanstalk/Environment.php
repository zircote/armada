<?php

namespace Armada\Resource\AWS\ElasticBeanstalk;

use Armada\Resource\AbstractResource;

class Environment extends AbstractResource
{
    const NAME = 'AWS::ElasticBeanstalk::Environment';
    /**
     * The name of the application associated with this environment.
     *
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $applicationName;
    /**
     * The URL to the CNAME for this environment.
     *
     * Required: No
     * Type: String
     *
     * @var string
     */
    protected $cnamePrefix;
    /**
     * Required: No
     * Type: String
     *
     * @var string
     */
    protected $description;
    /**
     * The option settings to add.
     *
     * Required: No
     * Type: A list of OptionSettings.
     *
     * @var \OptionSettings
     */
    protected $optionSettings;
    /**
     * The option settings to remove.
     *
     * Required: No
     * Type: A list of OptionSettings.
     *
     * @var \OptionSettings
     */
    protected $optionsToRemove;
    /**
     * The stack name associated with the environment.
     *
     * Required: No
     * Type: String
     *
     * @var string
     */
    protected $solutionStackName;
    /**
     * The name of the template to use with the environment.
     *
     * Required: No
     * Type: String
     *
     * @var string
     */
    protected $templateName;
    /**
     * The version to associate with the environment.
     *
     * Required: No
     * Type: String
     *
     * @var string
     */
    protected $versionLabel;

}

