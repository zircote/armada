<?php

namespace Armada\Resource\AWS\ElasticBeanstalk;

use Armada\Resource\AbstractResource;

class Application extends AbstractResource
{
    const NAME = 'AWS::ElasticBeanstalk::Application';
    /**
     * Application versions associated with this application. An application version is a specific, labeled iteration of
     * deployable code.
     *
     * Required: Yes
     * Type: A list of ApplicationVersions.
     *
     * @var ApplicationVesions
     */
    protected $applicationVersions;
    /**
     * Configuration templates associated with this application. You can use templates to deploy different versions of
     * an application using the configuration settings defined in the template.
     *
     * Required: No
     * Type: A list of ConfigurationTemplates.
     *
     * @var \ConfigurationTemplate
     */
    protected $ConfigurationTemplates;
    /**
     * An optional description of this application.
     *
     * Required: No
     * Type: A list of ConfigurationTemplates.
     *
     * @var string
     */
    protected $Description;

}

