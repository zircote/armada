<?php

namespace Armada\Resource\AWS\S3;

use Armada\Resource\AbstractResource;

class BucketPolicy extends AbstractResource
{
    const NAME = 'AWS::S3::BucketPolicy';
    /**
     * A policy document containing permissions to add to the specified bucket.
     * Type: JSON Document
     * Required: Yes
     *
     * @var JSON(object)
     */
    protected $PolicyDocument;
    /**
     * Name of the bucket to which you want to add the policy.
     * Type: String
     * Required: Yes
     * @var string
     */
    protected $Bucket;


}

