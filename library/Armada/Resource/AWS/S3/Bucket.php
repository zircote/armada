<?php

namespace Armada\Resource\AWS\S3;

use Armada\Resource\AbstractResource;

class Bucket extends AbstractResource
{
    const NAME = 'AWS::S3::Bucket';

    /**
     * A canned ACL that grants predefined permissions on the bucket. Default is Private. For more information about
     * canned ACLs, see Canned ACLs in the Amazon S3 documentation.
     *
     * Type: String
     * Required: No
     * Valid values for AccessControl:
     *      - Private
     *      - PublicRead
     *      - PublicReadWrite
     *      - AuthenticatedRead
     *      - BucketOwnerRead
     *      - BucketOwnerFullControl
     *
     * @var string
     */
    protected $AccessControl;
    /**
     * Information used to configure the bucket as a static website. For more information, see Hosting Websites on
     * Amazon S3.
     *
     * Type: Website Configuration Type
     * Required: No
     *
     * @var WebsiteConfigurationType
     */
    protected $WebsiteConfiguration;
}

