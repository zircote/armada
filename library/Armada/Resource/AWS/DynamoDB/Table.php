<?php

namespace Armada\Resource\AWS\DynamoDB;

class Table
{
    const NAME = 'AWS::DynamoDB::Table';
    /**
     * The primary key structure for the table, consisting of a required HashKeyElement and an optional RangeKeyElement,
     * required only for composite primary keys. For more information about primary keys, see DynamoDB Primary Key.
     *
     * Required: Yes
     * Type: DynamoDB Primary Key
     *
     * Update requires: replacement
     *
     * @var \PrimaryKey
     */
    protected $keySchema;
    /**
     * New throughput for the specified table, consisting of values for ReadCapacityUnits and WriteCapacityUnits. For
     * more information about the contents of a Provisioned Throughput structure, see DynamoDB Provisioned Throughput.
     *
     * Required: Yes
     * Type: DynamoDB Provisioned Throughput
     *
     * Update requires: replacement
     *
     * @var \ProvisionedThroughput
     */
    protected $provisionedThroughput;

}

