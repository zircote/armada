<?php

namespace Armada\Resource\AWS\ElastiCache;

use Armada\Resource\AbstractResource;

class SecurityGroupIngress extends AbstractResource
{
    const NAME = 'AWS::ElastiCache::SecurityGroupIngress';
    /**
     * The name of the Cache Security Group to authorize.
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $cacheSecurityGroupName;
    /**
     * Name of the EC2 Security Group to include in the authorization.
     * Type: String
     * Required: Yes
     * @var string
     */
    protected $ec2SecurityGroupName;
    /**
     * Specifies the AWS Account ID of the owner of the EC2 security group specified in the EC2SecurityGroupName
     * property. The AWS Access Key ID is not an acceptable value.
     *
     * Required: No
     * Type: String
     *
     * @var string
     */
    protected $ec2SecurityGroupOwnerId;

}

