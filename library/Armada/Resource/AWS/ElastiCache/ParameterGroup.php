<?php

namespace Armada\Resource\AWS\ElastiCache;

use Armada\Resource\AbstractResource;

class ParameterGroup extends AbstractResource
{
    const NAME = 'AWS::ElastiCache::ParameterGroup';
    /**
     * The name of the cache parameter group family that the cache parameter group can be used with.
     *
     * Required: Yes
     * Type: String
     * @var string
     */
    protected $cacheParameterGroupFamily;
    /**
     * The description for the Cache Parameter Group.
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $description;
    /**
     * A comma-delimited list of parameter name/value pairs. For more information, go to ModifyCacheParameterGroup in
     * the Amazon ElastiCache API Reference Guide.
     *
     * Required: No
     * Type: String
     *
     * @var array(string)
     */
    protected $properties = array();

}

