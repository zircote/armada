<?php

namespace Armada\Resource\AWS\ElastiCache;

use Armada\Resource\AbstractResource;

class CacheCluster extends AbstractResource
{
    const NAME = 'AWS::ElastiCache::CacheCluster';
    /**
     * Indicates that minor engine upgrades will be applied automatically to the cache cluster during the maintenance
     * window.
     *
     * Required: No
     * Type: Boolean
     * Default: true
     *
     * Update requires: replacement
     *
     * @var boolean
     */
    protected $autoMinorVersionUpgrade;
    /**
     * The compute and memory capacity of nodes in a cache cluster.
     *
     * Required: Yes
     * Type: String
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $cacheNodeType;
    /**
     * The name of the cache parameter group associated with this cache cluster.
     *
     * Required: No
     * Type: String
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $cacheParameterGroupName;
    /**
     * A list of cache security group names associated with this cache cluster.
     * Required: Yes
     * Type: List of Strings
     *
     * Update requires: no interruption
     *
     * @var array(string)
     */
    protected $cacheSecurityGroupNames = array();
    /**
     * The name of the cache engine to be used for this cache cluster.
     *
     * Required: Yes
     * Type: String
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $engine;
    /**
     * The version of the cache engine to be used for this cluster.
     *
     * Required: No
     * Type: String
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $engineVersion;
    /**
     * The Amazon Resource Name (ARN) of the Amazon Simple Notification Service (SNS) topic to which notifications will
     * be sent.
     *
     * Required: No
     * Type: String
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $notificationTopicArn;
    /**
     * The number of cache nodes the cache cluster should have.
     *
     * Required: No
     * Type: String
     *
     * Update requires: some interruptions
     *
     * @var
     */
    protected $numCacheNodes;
    /**
     * The port number on which each of the cache nodes will accept connections.
     *
     * Required: No
     * Type: Integer
     *
     * Update requires: no interruption
     *
     * @var number
     */
    protected $port;
    /**
     * The EC2 Availability Zone that the cache cluster will be created in.
     *
     * Required: No
     * Type: String
     *
     * Update requires: replacement
     *
     * @var string
     */
    protected $preferredAvailabilityZone;
    /**
     * The weekly time range (in UTC) during which system maintenance can occur.
     *
     * Required: No
     * Type: String
     *
     * Update requires: no interruption
     *
     * @var string
     */
    protected $preferredMaintenanceWindow;

}

