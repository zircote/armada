<?php

namespace Armada\Resource\AWS\ElastiCache;

use Armada\Resource\AbstractResource;

class SecurityGroup extends AbstractResource
{
    const NAME = 'AWS::ElastiCache::SecurityGroup';
    /**
     * The description property of the new cache security group.
     * Required: Yes
     * Type: String
     *
     * @var string
     */
    protected $description;

}

