<?php
namespace Armada\Resource;

/**
 * @package
 * @category
 * @subcategory
 */
/**
 * @package
 * @category
 * @subcategory
 */
abstract class AbstractResource
{

    /**
     * The DependsOn attribute enables you to specify that the creation of a specific resource follows another. When
     * you add a DependsOn attribute to a resource, you specify that that resource is created only after the creation
     * of the resource specified in the DependsOn attribute. When you use wait conditions, you typically use the
     * DependsOn attribute on another resource to determine when the wait condition goes into effect. For more
     * information, see Creating Wait Conditions in a Template. However, you can use the DependsOn attribute on any
     * resource.
     *
     * For example, the following template contains an AWS::EC2::Instance resource with a DependsOn attribute that
     * specifies myDB, an AWS::RDS::DBInstance. When AWS CloudFormation creates this stack, it first creates myDB,
     * then creates Ec2Instance.
     *
     * @var array
     */
    protected $dependsOn = array();
    /**
     * Delete:
     *      Default. This policy directs AWS CloudFormation to delete the resource and all its content if applicable
     *      during stack deletion. You can add this deletion policy to any resource type.
     *      Note: For S3 buckets, deletion will succeed only if the bucket is empty.
     * Retain:
     *      This policy directs AWS CloudFormation to keep the resource without deleting the resource or its contents,
     *      as applicable, during stack deletion. You can add this deletion policy to any resource type. Note that when
     *      AWS CloudFormation completes the stack deletion, the stack will be in Delete_Complete state; however,
     *      resources with a Retain policy will continue to exist and will continue to incur applicable charges until
     *      you delete those resources.
     * Snapshot:
     *      This policy is allowed only for resources that support snapshots: AWS::RDS::DBInstance and AWS::EC2::Volume.
     *      This policy directs AWS CloudFormation to create a snapshot for the resource before deleting the resource.
     *      Note that when AWS CloudFormation completes the stack deletion, the stack will be in the Delete_Complete
     *      state; however, the snapshots created with this policy will continue to exist and continue to incur
     *      applicable charges until you delete those snapshots.
     * @var
     */
    protected $deletionPolicy;
    /**
     * The Metadata attribute enables you to associate structured data with a resource. By adding a Metadata attribute
     * to a resource, you can add data in JSON format to the resource declaration. In addition, you can use intrinsic
     * functions (such as GetAtt and Ref), parameters, and pseudo parameters within the Metadata attribute to add those
     * interpreted values. AWS CloudFormation does not validate the JSON in the Metadata attribute.
     *
     * You can retrieve this data using the cfn-describe-stack-resource command or the DescribeStackResource action.
     *
     * The following template contains an Amazon S3 bucket resource with a Metadata attribute.
     * <code>
     * {
     *     "AWSTemplateFormatVersion" : "2010-09-09",
     *     "Resources" : {
     *        "MyS3Bucket" : {
     *          "Type" : "AWS::S3::Bucket",
     *          "Metadata" : { "Object1" : "Location1",  "Object2" : "Location2" }
     *        }
     *     }
     * }
     * </code>
     * @var array
     */
    protected $metadata = array();
}
